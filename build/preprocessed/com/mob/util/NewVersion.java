/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mob.util;

import com.mob.midlet.Attendance;
import javax.microedition.io.ConnectionNotFoundException;

/**
 *
 * @author hudson
 */
public class NewVersion implements Runnable{
    private Attendance midlet;
    
    public void startUpdate(){
        Thread th = new Thread(this);
        th.start();
    }
    
    public NewVersion(Attendance mob){
        this.midlet = mob;
    }

    public void run() {
        try {
            midlet.platformRequest(Update.upgradeURL);
        } catch (ConnectionNotFoundException ex) {
            midlet.getAlert().setTitle("Error");
            midlet.getAlert().setString(ex.getMessage());
            midlet.switchDisplayable(null, midlet.getAlert());
        }
        catch (Exception ex) {
            midlet.getAlert().setTitle("Error");
            midlet.getAlert().setString(ex.getMessage());
            midlet.switchDisplayable(null, midlet.getAlert());
        }
    }
}
