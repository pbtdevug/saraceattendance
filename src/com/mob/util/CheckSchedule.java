/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mob.util;

import com.mob.midlet.Attendance;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.AlertType;
import javax.microedition.rms.RecordStore;
import org.kxml.Xml;
import org.kxml.io.ParseException;
import org.kxml.parser.ParseEvent;
import org.kxml.parser.XmlParser;

/**
 *
 * @author hudson
 */
public class CheckSchedule implements Runnable {
    private InputStream is = null;
    private OutputStream os = null;
    private HttpConnection conn = null;
    private Attendance midlet;
    public static final String submit = "CHECK";
    private String today,status,connectionURL,imei,currentDateTime,phoneNum,id="",shift,upSignature,txNum,pin,location="",name="",confNum="",locationID,replyRootTag = "reply";;
    
    public CheckSchedule(Attendance midlet){
        this.midlet = midlet;
        today = midlet.up.today;
    }
    
    public void startConnection(){
        Thread th = new Thread(this);
        status = null;
        th.start();
        midlet.serverConnectionSheduler();
    }
    
    public void setTransaction(String name,String id,String shift,String pin,String location,String locationID,String trxnNum,String url,String imei){
        this.name = name;
        this.id = id;
        this.shift = shift;
        this.pin = pin;
        this.locationID = locationID;
        this.location = location;
        this.txNum = trxnNum;
        this.connectionURL = url;
        this.imei = imei;
    }

    public void run() {
        String url = connectionURL;
        System.out.println(url);
        int respCode = 0;
        byte [] bytes = createTransactionXML(name,id,shift,pin,location,txNum,imei).getBytes();
        
        try {
            conn = (HttpConnection)Connector.open(url,Connector.READ_WRITE,true);
            conn.setRequestMethod(HttpConnection.POST);
            conn.setRequestProperty("User-Agent","Profile/MIDP-2.1 Confirguration/CLDC-1.1");
            conn.setRequestProperty("Content-Language", "en-CA");
            conn.setRequestProperty("Content-Type","text/xml");
            os = conn.openOutputStream();
            os.write(bytes, 0, bytes.length);//write transaction to outputstream
            try{
                respCode = conn.getResponseCode();
                System.out.println(respCode);
            }
            catch(ConnectionNotFoundException ex){
                midlet.serverConnectionTimer.cancel();
                today = midlet.up.today;
                midlet.connectionIsAvailable = false;
                midlet.connectionIsAvailable = false;
                midlet.getAlert().setType(AlertType.ERROR);
                midlet.getAlert().setTitle("Connection Error");
                midlet.getAlert().setString("can't establish server connection!");
                midlet.switchDisplayable(midlet.getAlert(),midlet.confirmDetails());
            }
            catch(IOException ex){
                midlet.serverConnectionTimer.cancel();
                today = midlet.up.today;
                midlet.connectionIsAvailable = false;
                midlet.connectionIsAvailable = false;
                midlet.getAlert().setType(AlertType.ERROR);
                midlet.getAlert().setTitle("Connection Error");
                midlet.getAlert().setString("can't connect to server");
                midlet.switchDisplayable(midlet.getAlert(),midlet.confirmDetails());
            }
            if (respCode == HttpConnection.HTTP_OK){
                midlet.connectionIsAvailable = true;
                is = conn.openInputStream();
                
                /*int ch ;
                StringBuffer sb = new StringBuffer();
                while((ch = is.read())!=-1){
                    sb.append((char)ch);
                }
                System.out.println(sb.toString());*/

                viewXML(is);
                midlet.serverConnectionTimer.cancel();
                //System.out.println(midlet.up.guardHashtable.toString());
                if(upSignature!= null){
                    RecordStore.deleteRecordStore("guardRecords");//remove previous records
                    byte [] byts = (upSignature+"*"+midlet.up.guardHashtable.toString()+"*"+midlet.up.locationsHashTable.toString()+"*"+midlet.up.itemsHashtable.toString()).getBytes();//get byte representation
                    RecordStore rst = RecordStore.openRecordStore("guardRecords", true);
                    rst.addRecord(byts, 0, byts.length);
                    rst.closeRecordStore();//save update guard records
                }
                if(today==null||today.equals("null")){
                    today = midlet.up.today;
                }
                else{
                    today = Attendance.split(currentDateTime, " ").elementAt(0).toString();
                }
                midlet.commitStringItem().setText(status);
                if(shift.equals("DAY")||shift.equals("NIGHT")||shift.equals("DAY+NIGHT")){
                    midlet.commitStringItem2().setText("ID:"+id+"-"+name+"-"+shift+" shift at site:"+location);
                }
                else{
                    midlet.commitStringItem2().setText("ID:"+id+"-"+name+"-"+shift+" shift");
                }
                midlet.switchDisplayable(null,midlet.commitForm());
            }
            else{
                midlet.connectionIsAvailable = false;
                midlet.hisPendFlag = false;
                today = midlet.up.today;
                status = "";
                midlet.getAlert().setType(AlertType.ERROR);
                midlet.getAlert().setTitle("Server Error");
                midlet.getAlert().setString("can't establish server connection!");
                midlet.switchDisplayable(midlet.getAlert(),midlet.confirmDetails());
            }
        }
        catch(ParseException ex){
            //ex.printStackTrace();
            midlet.serverConnectionTimer.cancel();
            midlet.getAlert().setType(AlertType.ERROR);
            midlet.getAlert().setTitle("Error");
            midlet.getAlert().setString("XML Parse Error!");
            midlet.switchDisplayable(midlet.getAlert(),midlet.mainMenu());
        }
        catch (ConnectionNotFoundException ex) {
            //ex.printStackTrace();
            midlet.serverConnectionTimer.cancel();
            today = midlet.up.today;
            midlet.connectionIsAvailable = false;
            midlet.getAlert().setType(AlertType.ERROR);
            midlet.getAlert().setTitle("Connection Error");
            midlet.getAlert().setString("can't establish server connection!");
            midlet.switchDisplayable(midlet.getAlert(),midlet.confirmDetails());
        }
        catch (IOException ex) {
            //ex.printStackTrace();
            midlet.serverConnectionTimer.cancel();
            today = midlet.up.today;
            midlet.connectionIsAvailable = false;
            midlet.connectionIsAvailable = false;
            midlet.getAlert().setType(AlertType.ERROR);
            midlet.getAlert().setTitle("Connection Error");
            midlet.getAlert().setString("can't establish server connection!");
            midlet.switchDisplayable(midlet.getAlert(),midlet.confirmDetails());
        }
        catch (Exception ex) {
            midlet.getAlert().setType(AlertType.ERROR);
            midlet.getAlert().setTitle("Error");
            midlet.getAlert().setString("Application Error");
            midlet.switchDisplayable(midlet.getAlert(),midlet.confirmDetails());
            //ex.printStackTrace();
        }
        finally{
            if(is!=null){
                try {
                    is.close();
                }
                catch (IOException ex) {
                    /*midlet.getAlert().setType(AlertType.ERROR);
                    midlet.serverConnectionTimer.cancel();midlet.getAlert().setTitle("Error");
                    midlet.switchDisplayable(midlet.getAlert(),midlet.confirmDetails());
                    midlet.getAlert().setString("hudspon");*/
                }
            }
            if(os!=null){
                try {
                    os.close();
                } 
                catch (IOException ex) {
                    /* midlet.getAlert().setType(AlertType.ERROR);
                    midlet.getAlert().setTitle("Error");
                    midlet.switchDisplayable(midlet.getAlert(),midlet.confirmDetails());
                    midlet.getAlert().setString(ex.getMessage());*/
                }
            }
            if(conn!=null){
                try {
                    conn.close();
                } 
                catch (IOException ex) {
                    /*midlet.getAlert().setType(AlertType.ERROR);
                    midlet.getAlert().setTitle("Error");
                    midlet.switchDisplayable(midlet.getAlert(),midlet.confirmDetails());
                    midlet.getAlert().setString(ex.getMessage());*/
                }
            }
        }
    }
    
    private void viewXML(InputStream is){
        ParseEvent pe;
        String locaction = "";
        String action= new String();
        String idt = new String();
        String number = new String();
        String gName = new String();
        String deploymentName = new String(),deploymentID = new String(),deploymentOfficerName = new String(),
                                deploymentOfficerID = new String(),deploymentOfficerMobileNumber= new String(),
                                officerDets,itemID = new String(),itemName = new String(),itemUnit = new String();
        
        try{
            InputStreamReader xmlReader = new InputStreamReader(is);
            XmlParser parser = new XmlParser( xmlReader );
            parser.skip();//skips the first line ie <?xml version='1.0' encoding='UTF-8'?>
            parser.read(Xml.START_TAG, null, replyRootTag);
            boolean trucking = true;
            while (trucking) {
                pe = parser.read();
                if (pe.getType() == Xml.START_TAG &&pe.getName().equals("transNo")) {
                    pe = parser.read();
                    txNum= pe.getText();
                    System.out.println("TrxnNum = "+txNum);
                }
                else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("confNo")) {
                    pe = parser.read();
                    confNum = pe.getText();
                    System.out.println("confNum = "+confNum);
                }
                else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("status")) {
                    pe = parser.read();
                    status= pe.getText();
                    System.out.println("Status = "+status);
                }
                else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("dateTime")) {
                    pe = parser.read();
                    currentDateTime= pe.getText();
                    System.out.println("date = "+currentDateTime);
                }
                else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("signature")){
                    pe = parser.read();
                    upSignature = pe.getText();
                    System.out.println("signature = "+upSignature);
                }
                else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("update")){
                    String nemu = pe.getName();
                    if (nemu.equals("update")) {
                        while((pe.getType() != Xml.END_TAG) || (pe.getName().equals(nemu) == false)){
                            pe = parser.read();
                            if (pe.getType() == Xml.START_TAG&&pe.getName().equals("record")){
                                String name1 = pe.getName();
                                if (name1.equals("record")) {
                                    while ((pe.getType() != Xml.END_TAG) || (pe.getName().equals(name1) == false)) {
                                        pe = parser.read();
                                        if (pe.getType() == Xml.START_TAG &&pe.getName().equals("name")) {
                                            pe = parser.read();
                                            gName = pe.getText();
                                            name = gName;
                                            System.out.println("update Name:"+name);
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("number")) {
                                            pe = parser.read();
                                            number = pe.getText();
                                            phoneNum = number;
                                            System.out.println("update num:"+phoneNum);
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("id")) {
                                            pe = parser.read();
                                            idt = pe.getText();
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                            pe = parser.read();
                                            action = pe.getText();
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("location")) {
                                            pe = parser.read();
                                            locaction = pe.getText();
                                        }
                                    }
                                    //hashtable for guards
                                    if("INSERT".equalsIgnoreCase(action)){
                                        if(number==null||locaction==null){
                                            number = "null";
                                            locaction = "null";
                                        }
                                        midlet.up.guardHashtable.put(idt.trim(), gName.trim()+"_"+number.trim()+"_"+locaction.trim());
                                    }
                                    else if("DELETE".equalsIgnoreCase(action)){
                                        midlet.up.guardHashtable.remove(idt.trim());
                                    }
                                }
                                else {
                                    while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(name1) == false))
                                        pe = parser.read();
                                }
                            }
                            else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("site")){
                                String nam = pe.getName();
                                if (nam.equals("deployment")) {
                                    while ((pe.getType() != Xml.END_TAG) || (pe.getName().equals(nam) == false)) {
                                        pe = parser.read();
                                        if (pe.getType() == Xml.START_TAG &&pe.getName().equals("siteName")) {
                                            pe = parser.read();
                                            deploymentName = pe.getText();
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                            pe = parser.read();
                                            action = pe.getText();
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("siteID")) {
                                            pe = parser.read();
                                            deploymentID = pe.getText();
                                        }
                                    }   
                                    //hashtable for locations
                                    officerDets = deploymentOfficerName.trim()+"#"+deploymentOfficerID.trim()+"#"+deploymentOfficerMobileNumber.trim()+"#"+deploymentName.trim();
                                    if("INSERT".equalsIgnoreCase(action)){
                                        midlet.up.locationsHashTable.put(deploymentID.trim(), officerDets);
                                    }
                                    else if("DELETE".equalsIgnoreCase(action)){
                                        midlet.up.locationsHashTable.remove(deploymentID.trim());
                                    }
                                }
                                else {
                                    while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(nam) == false))
                                        pe = parser.read();
                                }
                            }
                        }
                    }
                    else {
                        while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(nemu) == false))
                            pe = parser.read();
                    }
                }
                if (pe.getType() == Xml.END_TAG &&pe.getName().equals(replyRootTag))
                    trucking = false;
            }
        }
        catch(Exception ex){
        }
    }
    
    private String createTransactionXML(String name,String id,String shift,String pin,String location,String txNum,String imei){
        //time = midlet.up.today;
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<record>");
        if(name.equals("Guard")){
            name = "";
            xmlStr.append("<name>").append(name).append("</name>");
        }
        xmlStr.append("<id>").append(id).append("</id>");
        xmlStr.append("<shift>").append(shift).append("</shift>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<siteID>").append(locationID).append("</siteID>");
        xmlStr.append("<siteName>").append(location).append("</siteName>");
        xmlStr.append("<transNo>").append(txNum).append("</transNo>");
        xmlStr.append("<signature>").append(midlet.up.updateSign).append("</signature>");
        xmlStr.append("<submit>").append(CheckSchedule.submit).append("</submit>");
        xmlStr.append("<imei>").append(imei).append("</imei>");
        xmlStr.append("<version>").append(Strings.Version).append("</version>");
        xmlStr.append("</record>");
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
}
