/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mob.util;

import com.mob.midlet.Attendance;

/**
 *
 * @author hudson
 */
public final class Strings {//http://192.168.1.24/askari/backend/mobile/mobile.php "http://162.144.57.3/cgi-bin/askari/mobile.redirect"; 
    private Attendance sar;
    public static final String image = "/com/mob/midlet/askari.png";
    public static final String hntURL = "http://www.askariplus.com/askari/backend/mobile/mobile.php";
    public static final String Version = "Tue Feb 17 11:42:34 UTC 2015";
    public static final String formsTitle = "Attendance";
    public static final String historyStoreName = "history";
    public static final String pendingStoreName = "pending";
    public static final String errorAlertTitle = "Error";
    public static final String historyChoiceGroupName = "History";
    public static final String noTrxnSelectedMsg = "There is no Transaction selected!";
    public static final String updateTickerMsg = "....checking for updates....Powered by PBT";
    public static final String waitFormStringitemText = "Askari Attendance";
    public static final String pinError = "Pin Error";
    public static final String pinErrorString = "pin must be 4 characters long!";
    public static final String emptyString = "";
    public static final String validationMsg = "checkPointID required!";
    public static final String passwordTextBoxTitle = "Enter your 4 digit PIN";
    public static final String method = "OTP";
    public static final String connectionNotFound = "Connection not Found!";
    
}
