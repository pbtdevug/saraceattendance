/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mob.util;

import com.mob.midlet.Attendance;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import javax.microedition.io.ConnectionNotFoundException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.rms.RecordStore;
import org.kxml.Xml;
import org.kxml.parser.ParseEvent;
import org.kxml.parser.XmlParser;

/**
 *
 * @author hudson
 */
public class SendPending implements Runnable {
    private Attendance midlet;
    private InputStream is = null;
    private OutputStream os = null;
    private HttpConnection conn = null;
    private String txNum,id="",shift,pin,location="",time,name="",confNum="",status,
            currentDateTime,today,imei,replyRootTag,upSignature,connectionURL,locationID,opt,pendingState,phoneNum;
    private StringBuffer ActiveTrxnSb = new StringBuffer();
    
    public SendPending(Attendance midlet){
        this.midlet = midlet;
    }
    
    public void startConnection(){
        Thread th = new Thread(this);
        th.start();
        status = null;
    }
    
    public void setTransaction(String name,String id,String shift,String pin,String location,String locationID,String trxnNum,String pendingState, String url,String imei){
        this.txNum = trxnNum;
        this.name = name;
        this.id = id;
        this.shift = shift;
        this.pin = pin;
        this.locationID = locationID;
        this.location = location;
        this.pendingState = pendingState;
        this.connectionURL = url;
        this.imei = imei;
    }
    
    public void run() {
        String url = connectionURL;
        byte [] bytes;
        int respCode = 0;
        replyRootTag = "reply";
        ParseEvent pe;
        
        bytes = createTransactionXML(name,id,shift,pin,location,txNum,imei).getBytes();
        
        try {
            conn = (HttpConnection)Connector.open(url,Connector.READ_WRITE,true);
            conn.setRequestMethod(HttpConnection.POST);
            conn.setRequestProperty("User-Agent","Profile/MIDP-2.1 Confirguration/CLDC-1.1");
            conn.setRequestProperty("Content-Language", "en-CA");
            conn.setRequestProperty("Content-Type","text/xml");
            os = conn.openOutputStream();
            os.write(bytes, 0, bytes.length);//write transaction to outputstream
            os.close();
            try{
                respCode = conn.getResponseCode();
            }
            catch(ConnectionNotFoundException ex){
                //ex.printStackTrace();
                status = "";
            }
            catch(IOException ex){
                status = "";
            }
            if (respCode == HttpConnection.HTTP_OK){
                midlet.connectionIsAvailable = true;
                is = conn.openInputStream();

                /* int ch ;
                StringBuffer sb = new StringBuffer();
                while((ch = is.read())!=-1){
                    sb.append((char)ch);
                }
                System.out.println(sb.toString());*/

                String locaction = "";
                String action= new String();
                String idt = new String();
                String number = new String();
                //String gName = new String();
                String deploymentName = new String(),deploymentID = new String(),deploymentOfficerName = new String(),
                        deploymentOfficerID = new String(),deploymentOfficerMobileNumber= new String(),
                        officerDets,itemID = new String(),itemName = new String(),itemUnit = new String();

                InputStreamReader xmlReader = new InputStreamReader(is);
                XmlParser parser = new XmlParser( xmlReader );
                parser.skip();//skips the first line ie <?xml version='1.0' encoding='UTF-8'?>
                parser.read(Xml.START_TAG, null, replyRootTag);
                boolean trucking = true;
                while (trucking) {
                    pe = parser.read();
                    if (pe.getType() == Xml.START_TAG &&pe.getName().equals("transNo")) {
                        pe = parser.read();
                        txNum= pe.getText();
                    }
                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("confNo")) {
                        pe = parser.read();
                        confNum = pe.getText();
                    }
                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("status")) {
                        pe = parser.read();
                        status= pe.getText();
                    }
                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("dateTime")) {
                        pe = parser.read();
                        currentDateTime= pe.getText();
                    }
                    else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("signature")){
                        pe = parser.read();
                        upSignature = pe.getText();
                    }
                    else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("update")){
                        String nemu = pe.getName();
                        if (nemu.equals("update")) {
                            while((pe.getType() != Xml.END_TAG) || (pe.getName().equals(nemu) == false)){
                                pe = parser.read();
                                /*if(pe.getType() == Xml.START_TAG &&pe.getName().equals("signature")){
                                     pe = parser.read();
                                     upSignature = pe.getText();
                                }
                                else*/ if (pe.getType() == Xml.START_TAG&&pe.getName().equals("record")){
                                    String name1 = pe.getName();
                                    if (name1.equals("record")) {
                                        while ((pe.getType() != Xml.END_TAG) || (pe.getName().equals(name1) == false)) {
                                            pe = parser.read();
                                            if (pe.getType() == Xml.START_TAG &&pe.getName().equals("name")) {
                                                pe = parser.read();
                                                String nme = pe.getText();
                                                if(nme == null){
                                                    name = "null";
                                                }
                                                else{
                                                    name = nme;
                                                }
                                            }
                                            else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("number")) {
                                                pe = parser.read();
                                                number = pe.getText();
                                                phoneNum = number;
                                            }
                                            else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("id")) {
                                                pe = parser.read();
                                                idt = pe.getText();
                                            }
                                            else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                                pe = parser.read();
                                                action = pe.getText();
                                            }
                                            else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("location")) {
                                                pe = parser.read();
                                                locaction = pe.getText();
                                            }
                                        }
                                        //hashtable for guards
                                        if("INSERT".equalsIgnoreCase(action)){
                                            if(number==null||locaction==null){
                                                number = "null";
                                                locaction = "null";
                                            }
                                            midlet.up.guardHashtable.put(idt.trim(), name.trim()+"_"+number.trim()+"_"+locaction.trim());
                                        }
                                        else if("DELETE".equalsIgnoreCase(action)){
                                            midlet.up.guardHashtable.remove(idt.trim());
                                        }
                                    }
                                    else {
                                        while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(name1) == false))
                                        pe = parser.read();
                                    }
                                }
                                else if(pe.getType() == Xml.START_TAG&&pe.getName().equals("site")){
                                    String nam = pe.getName();
                                    if (nam.equals("deployment")) {
                                        while ((pe.getType() != Xml.END_TAG) || (pe.getName().equals(nam) == false)) {
                                            pe = parser.read();
                                            if (pe.getType() == Xml.START_TAG &&pe.getName().equals("siteName")) {
                                                pe = parser.read();
                                                deploymentName = pe.getText();
                                            }
                                            else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                                pe = parser.read();
                                                action = pe.getText();
                                            }
                                            else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("siteID")) {
                                                pe = parser.read();
                                                deploymentID = pe.getText();
                                            }
                                        }
                                        //hashtable for locations
                                        officerDets = deploymentOfficerName.trim()+"#"+deploymentOfficerID.trim()+"#"+deploymentOfficerMobileNumber.trim()+"#"+deploymentName.trim();
                                        if("INSERT".equalsIgnoreCase(action)){
                                            midlet.up.locationsHashTable.put(deploymentID.trim(), officerDets);
                                        }
                                        else if("DELETE".equalsIgnoreCase(action)){
                                            midlet.up.locationsHashTable.remove(deploymentID.trim());
                                        }
                                    }
                                    else {
                                        while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(nam) == false))
                                        pe = parser.read();
                                    }
                                }
                            }
                        }
                        else {
                            while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(nemu) == false))
                                pe = parser.read();
                        }

                    }
                    if (pe.getType() == Xml.END_TAG &&pe.getName().equals(replyRootTag))
                            trucking = false;
                }
                //System.out.println(status);
                if(upSignature!= null){
                    RecordStore.deleteRecordStore("guardRecords");//remove previous records
                    byte [] byts = (upSignature+"*"+midlet.up.guardHashtable.toString()+"*"+midlet.up.locationsHashTable.toString()+"*"+midlet.up.itemsHashtable.toString()).getBytes();//get byte representation
                    RecordStore rst = RecordStore.openRecordStore("guardRecords", true);
                    rst.addRecord(byts, 0, byts.length);
                    rst.closeRecordStore();//save update guard records
                }
                //System.out.println(status);
                today = Attendance.split(currentDateTime, " ").elementAt(0).toString();
                if(status.equalsIgnoreCase("SUCCESSFUL")){//.append("*").append(locationID)
                    ActiveTrxnSb.delete(0, ActiveTrxnSb.length());
                    ActiveTrxnSb.append(pendingState).append("*").append(name).append("*").append(id).append("*").append(shift).append("*").append(phoneNum).append("*").append(confNum).append("*").append(locationID).append("*").append(location).append("*").append(pin).append("*").append(status).append("*").append(today);
                    
                    RecordStore pendingRecordStore = RecordStore.openRecordStore("pending", true);
                    //delete transaction from pending
                    pendingRecordStore.deleteRecord(midlet.pendingRecordID);
                    pendingRecordStore.closeRecordStore();
                    midlet.connectionIsAvailable = true;
                    //save transaction to history
                    midlet.hisPendFlag = false;
                    //System.out.println(ActiveTrxnSb.toString());
                    midlet.saveTransaction(ActiveTrxnSb.toString());
                    midlet.connectionIsAvailable = true;
                }
                else{
                    ActiveTrxnSb.delete(0, ActiveTrxnSb.length());
                    
                    ActiveTrxnSb.append(pendingState).append("*").append(name).append("*").append(id).append("*").append(shift).append("*").append(phoneNum).append("*").append(confNum).append("*").append(locationID).append("*").append(location).append("*").append(pin).append("*").append(status).append("*").append(today);
                        
                    RecordStore pendingRecordStore = RecordStore.openRecordStore("pending", true);
                    //delete transaction from pending
                    pendingRecordStore.deleteRecord(midlet.pendingRecordID);
                    pendingRecordStore.closeRecordStore();
                    midlet.connectionIsAvailable = true;
                    //save transaction to history
                    midlet.hisPendFlag = false;
                    //System.out.println(ActiveTrxnSb.toString());
                    midlet.saveTransaction(ActiveTrxnSb.toString());
                    midlet.connectionIsAvailable = true;
                }
            }
            else{
                status = "";
                midlet.connectionIsAvailable = false;
            }
        }
        catch (IOException ex) {
            //ex.printStackTrace();
            status = "";
            midlet.connectionIsAvailable = false;
        }
        catch (Exception ex) {
            //ex.printStackTrace();
        }
        finally{
            if(is!=null){
                try {
                    is.close();
                } catch (IOException ex) {
                }
            }
            if(os!=null){
                try {
                    os.close();
                } 
                catch (IOException ex) {
                }
            }
            if(conn!=null){
                try {
                    conn.close();
                } catch (IOException ex) {
                }
            }
        }
    }
    
    public String createTransactionXML(String name,String id,String shift,String pin,String location,String txNum,String imei){
        time = midlet.up.today;
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<record>");
        if(name.equals("Guard")){
            name = "";
            xmlStr.append("<name>").append(name).append("</name>");
        }
        xmlStr.append("<id>").append(id).append("</id>");
        xmlStr.append("<shift>").append(shift).append("</shift>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<time>").append(time).append("</time>"); 
        xmlStr.append("<siteID>").append(locationID).append("</siteID>");
        xmlStr.append("<siteName>").append(location).append("</siteName>");
        xmlStr.append("<transNo>").append(txNum).append("</transNo>");
        xmlStr.append("<signature>").append(midlet.up.updateSign).append("</signature>");
        xmlStr.append("<imei>").append(imei).append("</imei>");
        xmlStr.append("<version>").append(Strings.Version).append("</version>");
        xmlStr.append("</record>");
        //System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
}
