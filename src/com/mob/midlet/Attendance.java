
package com.mob.midlet;

import com.mob.util.CheckSchedule;
import com.mob.util.ConnectToServer;
import com.mob.util.ListFilter;
import com.mob.util.NewVersion;
import com.mob.util.SendPending;
import com.mob.util.Strings;
import com.mob.util.Update;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;
import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;
import org.netbeans.microedition.lcdui.SplashScreen;

/**
 * @author hudson
 */
public class Attendance extends MIDlet implements CommandListener,ItemCommandListener,ItemStateListener{
    
//<editor-fold defaultstate="collapsed" desc=" Fields ">
    //javax.microedition.io.Connector.openDataInputStream("redirect://");
    private boolean midletPaused = false,pinFlag =false,pendingFlag,scheduleShifts=false;
    public boolean connectionIsAvailable = false,historyFlag = false,pendingSendFlag =false,runingFirstTime = false,hisPendFlag = false;
    private Display display;
    private Command exitCommand;
    private Command cancelCommand,commitCommand;
    private Command backCommand,newSubmissionCommand;
    private Command submitCommand,historyCommand,updateCommand,pendingCommand;
    private Command okCommand,mainMenuCommand,viewCommand,saveCommand,resetPasswordCommand;
    public Form successForm,actionsForm,checkPointForm,tourSuccessForm,state,configForm;
    private Alert alert,timeOutAlert;
    public Form mainMenu,confirmShiftsForm,confirmAttendanceForm,shiftsSuccessForm,confirmItemForm,shiftSubmission_locationsForm;
    private ChoiceGroup categoryChoiceGroup;
    private Form idEntryForm;
    private ChoiceGroup shiftChoiceGroup,actionsChoiceGroup;
    public Form confirmDetails,tourConfirmForm,itemSuccessForm,attSuccessForm,storelocationsForm;
    public Form waitForm,incorrectPinForm,itemsForm,pinOtpForm;
    public Form historyPendingForm,commitForm,passWordForm,expectedShiftsForm,clockIncidentForm,scheduleShiftLocationsForm;
    private ChoiceGroup historyPendingChoiceGroup,clocksChoiceGroup,operationsChoiceGroup;
    private SplashScreen splashScreen;
    private Ticker ticker;
    public StringBuffer activeTrxn = new StringBuffer();
    public Vector categ = new Vector(),itemsVector = new Vector();
    private Vector det = new Vector(), recordIDs = new Vector();
    public Hashtable requiredFields = new Hashtable(),locationANDid = new Hashtable(),itemANDid= new Hashtable();
    public Update up = new Update(this);
    public NewVersion nv = new NewVersion(this);
    private ConnectToServer cts = new ConnectToServer(this);
    private CheckSchedule cs = new CheckSchedule(this);
    public SendPending sp = new SendPending(this);
    private String shift,location,pin,recordID,choiceGroup,pwd,expShifts,adj,selectedAction,checkPointID;
    public RecordStore pending,history,updateSignature,password;
    public String paymentType,connectionUrl,reciept,confNumber=Strings.emptyString,signature,selectedOperation,number,selectedLocation,officer,phoneNum,selectedLocationID,description,id,name,otp,imei;
    public Timer timer,timer1,timer2,serverConnectionTimer;
    public int pendingRecordID;
    private Font font;
    private StringItem time,commitStringItem,commitStringItem2,commitStringItem3,locationStringItem,phoneStringItem,officerStringItem,time1,clockTimeStringItem,timeStringItem2,shiftLocationStringItem,itemtTmeStringItem;
    private TextField idTextField,imeiTextField,connURLTextField,attDescription,actionsOTPTextBox,actionsPINTextBox,staffIDTextBox,actionsCheckPointIDTextBox,searchTextField,descriptionTextField,shiftsTextField,adjTextField,checkPointIDTextField,itemNameTextField;
    private TextBox tourPinTextBox,supportForm,passwordTextBox,itemQtyTextField,shiftPinTextBox,scheduleShiftPasswordTextBox,itemPinTextBox,schedulePinTextBox,optPinTextBox;
    private ListFilter locationsChoiceGroup,itemsChoiceGroup;
    private Image iconImage;
    public String itemUnit,itemID,itemQty,url;
    public Spacer spacer ; 
    
    private RecordStore rst;
    private int numRecords = 0;
    //1101
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" Attendance ">
    public Attendance(){
        String config;
        System.out.println("constractor inoked");
        try{
            rst = RecordStore.openRecordStore("config", true);
            numRecords = rst.getNumRecords();
            if(numRecords>0){
                RecordEnumeration re = rst.enumerateRecords(null, null, false);
                byte [] byts = null;
                while(re.hasNextElement()){
                    byts = re.nextRecord();
                    break;
                }
                config = new String(byts,0,byts.length);
                
                System.out.println("config String = "+config);
                
                Vector contents = split(config,"*");
                
                imei = contents.elementAt(0).toString();
                System.out.println(imei);
                
                connectionUrl = contents.elementAt(1).toString();
                url = connectionUrl;
                System.out.println(url);
            }
            rst.closeRecordStore();
        }
        catch(RecordStoreException ex){
            ex.printStackTrace();
        }
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" initialize ">
    private void initialize() {
        //System.out.println(getDateTime());
        pendingSheduler();//start scheduler for pending transactions
        //updateSheduler();//start scheduler for updating
        deleteSheduler();// start scheduler for deleting history
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" startMIDlet ">
    public void startMIDlet() {
        if(numRecords>0){
            System.out.println(new Date().toString());
            switchDisplayable(null, getSplashScreen());// display first screen
        }
        else{
           switchDisplayable(null, configForm()); 
        }
        
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" resumeMIDlet ">
    public void resumeMIDlet() {
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" switchDisplayable ">
    /**
     * Switches a current displayable in a display. The
     * <code>display</code> instance is taken from
     * <code>getDisplay</code> method. This method is used by all actions in the
     * design for switching displayable.
     *
     * @param alert the Alert which is temporarily set to the display;
     * if <code>null</code>, then <code>nextDisplayable</code> is set
     * immediately
     * @param nextDisplayable the Displayable to be set
     */
    public void switchDisplayable(Alert alert, Displayable nextDisplayable) {
        display = getDisplay();
        if (alert == null) {
            display.setCurrent(nextDisplayable);//display intended screen
        } else {
            display.setCurrent(alert, nextDisplayable);//display alert first for some time and the intented screen
        }
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" commandAction for Displayables ">
    public void commandAction(Command command, Displayable displayable) {
        if(displayable == successForm||displayable == shiftsSuccessForm||displayable == tourSuccessForm||displayable == itemSuccessForm||displayable == attSuccessForm){
            successFormsActions(command);
        }
        else if(displayable == historyPendingForm){
            /*if(command == backCommand){
                switchDisplayable(null, mainMenu());
            }
            else */if(command == cancelCommand){
                switchDisplayable(null, mainMenu());
                resetForms();
                //pinTextBox().setString("");
            }
            else if(command == viewCommand){
                try{
                    String storeName;
                    //record ID corresponds to the element in recordIDs' vector at selected index of either History or Pending choice groups
                    recordID = (recordIDs.elementAt(historyPendingChoiceGroup().getSelectedIndex())).toString();
                    choiceGroup = historyPendingChoiceGroup().getLabel();//can be "History" or "Pending"
                    if(choiceGroup.equals(Strings.historyChoiceGroupName)){
                        storeName = Strings.historyStoreName;
                    }
                    else{
                        storeName = Strings.pendingStoreName;
                    }
                    //System.out.println(recordID);
                    decodeTransaction(recordID,storeName);//converts transaction back to string format
                }
                catch(Exception ex){
                    showAlert(Strings.errorAlertTitle,Strings.noTrxnSelectedMsg);
                }
            }
            else{
                successFormsActions(command);
            }
        }
        else if(displayable == supportForm){
            if(command == mainMenuCommand){
                switchDisplayable(null, mainMenu());
            }
        }
        else if(displayable == mainMenu){
            if(command == submitCommand){
                mainMenuSubmitAction();
            }
            else if(command == exitCommand){
                exitMIDlet(); 
            }
            else if(command == historyCommand){
                historyPendingChoiceGroup().setLabel(Strings.historyChoiceGroupName);
                createHistory(); 
            }
            else if(command == pendingCommand){
                createPending();
            }
            else if(command == updateCommand){
                nv.startUpdate();
            }
        }
        else if(displayable == waitForm){
            if(command == cancelCommand){
                if(state == null){
                    switchDisplayable(null,passwordTextBox());
                }
                else{
                    switchDisplayable(null, state);
                }
            }
        }
        else if(displayable==passwordTextBox){
            pin = passwordTextBox.getString();
            passwordTextBox.setString(Strings.emptyString);
            if(command == exitCommand){
                exitMIDlet(); 
            }
            else if(command == okCommand){
                if(pin.length()==4){
                    waitForm().deleteAll();
                    getTicker().setString(Strings.updateTickerMsg);
                    waitForm().append(new StringItem(Strings.waitFormStringitemText,null));
                    switchDisplayable(null,waitForm());
                    up.startUpdate(pin,imei,url);// start update thread
                }
                else{
                    getAlert().setTitle(Strings.pinError);
                    getAlert().setString(Strings.pinErrorString);
                    switchDisplayable(getAlert(),passwordTextBox());
                }
            }
            else if(command == backCommand){
                switchDisplayable(null,state);
            }
        }
        else if(displayable == splashScreen){
            if(command == SplashScreen.DISMISS_COMMAND){
                switchDisplayable(null, passwordTextBox());
                passwordTextBox.setTitle(Strings.passwordTextBoxTitle);
            }
        }
        //Shift submission
        //section for shift submission step1
        else if (displayable == shiftSubmission_locationsForm){
            if(command == submitCommand){
                try{
                    selectedLocation = locationsChoiceGroup().getString(locationsChoiceGroup.getSelectedIndex());
                    System.out.println(selectedLocation);
                    selectedLocationID = locationANDid.get(selectedLocation).toString();
                    shiftLocationStringItem().setLabel("Site: "+selectedLocation);
                    shiftLocationStringItem().setText(null);
                    idTextField().setString(Strings.emptyString);
                    switchDisplayable(null, idEntryForm());
                }
                catch(Exception ex){
                    ex.printStackTrace();
                }
            }
            else if(command == cancelCommand){ // Main Menu
                switchDisplayable(null, mainMenu());
            }
            else if(command == backCommand){ // Main Menu
                switchDisplayable(null, mainMenu());
            }
        }
        //section for shift submission step 2
        else if(displayable == idEntryForm){
            if(command == backCommand){  // step 1
                switchDisplayable(null, shiftSubmission_locationsForm());
            }
            else if(command == submitCommand){ // step 3
                createConfirmationForm();
            }
            else if(command == cancelCommand){ //  Main Menu
                switchDisplayable(null, mainMenu());
                resetForms();
                //pinTextBox().setString("");
            }
        }
        //section for shift submission step 3
        else if(displayable == confirmDetails){
            if(command == backCommand){
                switchDisplayable(null, idEntryForm());
                confirmDetails.deleteAll();
            }
            else if(command == submitCommand){
                state = confirmDetails;
                cts.setTransaction(name, id, shift, pin, location,selectedLocationID, getTrxnNumber(),url,imei);
                cts.startConnection();
                createWaitForm("Checking against schedule.....Powered by Peak Bandwidth Technologies");
                //switchDisplayable(null, shiftPinTextBox());
            }
            else if(command == cancelCommand){
                switchDisplayable(null, mainMenu());
                resetForms();
                //pinTextBox().setString("");
            }
        }
        //section for shift submission step 4
        else if(displayable == commitForm){
            if(command == commitCommand){
                cts.setTransaction(name, id, shift, pin, location,selectedLocationID, getTrxnNumber(),url,imei);
                cts.startConnection();
                createWaitForm("Submitting Shift.....Powered by Peak Bandwidth Technologies");
            }
            else if(command == cancelCommand){
                switchDisplayable(null, mainMenu());
                resetForms();
            }
        }
        //configuration Form
        else if(displayable == configForm){
            if(command == saveCommand){
                imei = imeiTextField.getString();
                connectionUrl = connURLTextField.getString();
                
                if(imei.length()>= 5 && connectionUrl.length()>20){
                    url = connectionUrl;
                    String config = imei+"*"+connectionUrl;
                    byte[] bytes = config.getBytes();
                    try{
                        RecordStore rstr = RecordStore.openRecordStore("config", true);
                        rstr.addRecord(bytes, 0, bytes.length);
                        rstr.closeRecordStore();

                        System.out.println("config String = "+config);
                        
                        switchDisplayable(null, passwordTextBox());
                        passwordTextBox.setTitle(Strings.passwordTextBoxTitle);
                        
                    }
                    catch(RecordStoreException ex){
                        ex.printStackTrace();
                    }
                }
                else{
                    getAlert().setTitle("Validation Error");
                    getAlert().setString("Invalid IMEI or URL");
                    switchDisplayable(null, getAlert());
                }
            }
            else if(command == exitCommand){
                exitMIDlet();
            }
        }
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" validateFields ">    
    private boolean validateFields(String msg,String field){
        boolean passedValidation;
        if(field.length()==0){
            getAlert().setTitle("Field Error");
            getAlert().setString(msg);
            switchDisplayable(null,getAlert());
            passedValidation = false;
        }
        else{
            passedValidation = true;
        }
        return passedValidation;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" successFormsActions ">
    private void successFormsActions(Command command){
        System.out.println(state);
        if(state == confirmDetails){
            if(command == mainMenuCommand){
                switchDisplayable(null, mainMenu()); 
                resetForms();
            }
            else if(command == newSubmissionCommand){
                switchDisplayable(null, shiftSubmission_locationsForm()); 
                searchTextField().setString(Strings.emptyString);
                resetForms();
            }
            else if(command == backCommand){
                switchDisplayable(null, historyPendingForm());
            }
        }
        else if(state == confirmShiftsForm){
            if(command == mainMenuCommand){
                switchDisplayable(null, mainMenu()); 
                resetForms();
            }
            else if(command == newSubmissionCommand){
                switchDisplayable(null, scheduleShiftLocationsForm());
                searchTextField().setString(Strings.emptyString);
                resetForms();
            }
            else if(command == backCommand){
                switchDisplayable(null, historyPendingForm());
            }
        }
        else if(state == confirmItemForm){
            if(command == mainMenuCommand){
                switchDisplayable(null, mainMenu()); 
                resetForms();
            }
            else if(command == newSubmissionCommand){
                switchDisplayable(null, itemsForm());
                searchTextField().setString(Strings.emptyString);
                resetForms();
            }
            else if(command == backCommand){
                switchDisplayable(null, historyPendingForm());
            }
        }
        else if(state == tourConfirmForm){
            if(command == mainMenuCommand){
                switchDisplayable(null, mainMenu()); 
                resetForms();
            }
            else if(command == newSubmissionCommand){
                switchDisplayable(null, checkPointForm()); 
                checkPointIDTextField().setString(Strings.emptyString);
                resetForms();
            }
            else if(command == backCommand){
                switchDisplayable(null, historyPendingForm());
            }
        }
        else if(state == confirmAttendanceForm){
            if(command == mainMenuCommand){
                switchDisplayable(null, mainMenu()); 
                resetForms();
            }
            else if(command == newSubmissionCommand){
                switchDisplayable(null, actionsForm());
                staffIDTextBox().setString(Strings.emptyString);
                actionsCheckPointIDTextBox().setString(Strings.emptyString);
                attDescription().setString(Strings.emptyString);
                resetForms();
            }
            else if(command == backCommand){
                switchDisplayable(null, historyPendingForm());
            }
        }
    }
    //</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" commitCommand ">
    public Command commitCommand() {
        if (commitCommand == null) {
            commitCommand = new Command("Commit", Command.BACK, 0);
        }
        return commitCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" exitCommand ">
    public Command getExitCommand() {
        if (exitCommand == null) {
            exitCommand = new Command("Exit", Command.EXIT, 0);
        }
        return exitCommand;
    }
//</editor-fold>    

//<editor-fold defaultstate="collapsed" desc=" successForm ">
    public Form successForm() {
        if (successForm == null) {
            successForm = new Form("Attendance", new Item[]{});
            //successForm.addCommand(newSubmissionCommand());
            successForm.addCommand(mainMenuCommand());
            successForm.setCommandListener(this);
        }
        return successForm;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" attSuccessForm ">
    public Form attSuccessForm() {
        if (attSuccessForm == null) {
            attSuccessForm = new Form("Attendence Submission", new Item[]{});
            //successForm.addCommand(newSubmissionCommand());
            attSuccessForm.addCommand(mainMenuCommand());
            attSuccessForm.setCommandListener(this);
        }
        return attSuccessForm;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" cancelCommand ">
    public Command cancelCommand() {
        if (cancelCommand == null) {
            // write pre-init user code here
            cancelCommand = new Command("Cancel", Command.CANCEL, 0);
            // write post-init user code here
        }
        return cancelCommand;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" historyCommand ">    
    public Command historyCommand(){
        if(historyCommand == null){
            historyCommand = new Command(Strings.historyChoiceGroupName,Command.OK,0);
        }
        return historyCommand;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" pendingCommand ">
    public Command pendingCommand(){
        if(pendingCommand == null){
            pendingCommand = new Command("Pending",Command.OK,0);
        }
        return pendingCommand;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" updateCommand ">
    public Command updateCommand(){
        if(updateCommand == null){
            updateCommand = new Command("Upgrade",Command.OK,0);
        }
        return updateCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" saveCommand ">
    public Command saveCommand() {
        if (saveCommand == null) {
            // write pre-init user code here
            saveCommand = new Command("Save", Command.OK, 0);
            // write post-init user code here
        }
        return saveCommand;
    }
//</editor-fold>   
    
//<editor-fold defaultstate="collapsed" desc=" backCommand ">
    public Command getBackCommand() {
        if (backCommand == null) {
            backCommand = new Command("Back", Command.OK, 0);
        }
        return backCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" okCommand ">
    public Command okCommand() {
        if (okCommand == null) {
            okCommand = new Command("Ok", Command.OK, 0);
        }
        return okCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" mainMenuCommand ">
    public Command mainMenuCommand() {
        if (mainMenuCommand == null) {
            mainMenuCommand = new Command("Main Menu", Command.OK, 1);
        }
        return mainMenuCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" newSubmissionCommand ">
    public Command newSubmissionCommand() {
        if (newSubmissionCommand == null) {
            newSubmissionCommand = new Command("New Trxn", Command.OK, 1);
        }
        return newSubmissionCommand;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" viewCommand ">
    private Command viewCommand() {
        if (viewCommand == null) {
            viewCommand = new Command("View", Command.BACK, 1);
        }
        return viewCommand;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" submitCommand ">
    public Command submitCommand() {
        if (submitCommand == null) {
            submitCommand = new Command("Submit", Command.BACK, 1);
        }
        return submitCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" mainMenu ">
    public Form mainMenu() {
        if (mainMenu == null) {
            // write pre-init user code here
            mainMenu = new Form(Strings.formsTitle, new Item[]{applicationsChoiceGroup()});
            mainMenu.addCommand(getExitCommand());
            mainMenu.addCommand(submitCommand());
            mainMenu.addCommand(historyCommand());
            mainMenu.addCommand(pendingCommand());
            mainMenu.addCommand(updateCommand());
            mainMenu.setCommandListener(this);
            // write post-init user code here
        }
        return mainMenu;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="configForm">
    private Form configForm() {
        if (configForm == null) {
            // write pre-init user code here
            configForm = new Form("Enter Configurations", new Item[]{imeiTextField(),connURLTextField()});
            configForm.addCommand(getExitCommand());
            configForm.addCommand(saveCommand());
            configForm.setCommandListener(this);
            // write post-init user code here
        }
        return configForm;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" timeStringItem ">                                   
    private StringItem timeStringItem() {
        if (time == null) {                                 
            time = new StringItem(up.today, null, Item.PLAIN);  
        }                         
        return time;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" commitStringItem ">                                   
    public StringItem commitStringItem() {
        if (commitStringItem == null) {                                 
            commitStringItem = new StringItem("Original Schedule", Strings.emptyString, Item.PLAIN);  
        }                         
        return commitStringItem;
    }
//</editor-fold> 
    
//<editor-fold defaultstate="collapsed" desc="commitStringItem2">                                   
    public StringItem commitStringItem2() {
        if (commitStringItem2 == null) {                                 
            commitStringItem2 = new StringItem("Intended Schedule", Strings.emptyString, Item.PLAIN);  
        }                         
        return commitStringItem2;
    }
//</editor-fold> 
    
//<editor-fold defaultstate="collapsed" desc="commitStringItem3">                                   
    public StringItem commitStringItem3() {
        if (commitStringItem3 == null) {                                 
            commitStringItem3 = new StringItem("Do you want to COMMIT this change?", Strings.emptyString, Item.PLAIN);  
        }                         
        return commitStringItem3;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" clockTimeStringItem ">    
    public StringItem clockTimeStringItem() {
        if (clockTimeStringItem == null) {                                 
            // write pre-init user code here
            clockTimeStringItem = new StringItem(up.today, null, Item.PLAIN);  
            //time.setFont(getFont());
        }                         
        return clockTimeStringItem;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" timeStringItem2 ">    
    public StringItem timeStringItem2() {
        if (timeStringItem2 == null) {                                 
            // write pre-init user code here
            timeStringItem2 = new StringItem(up.today, null, Item.PLAIN);  
            //time.setFont(getFont());
        }                         
        return timeStringItem2;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" itemtTmeStringItem ">    
    public StringItem itemtTmeStringItem() {
        if (itemtTmeStringItem == null) {                                 
            // write pre-init user code here
            itemtTmeStringItem = new StringItem(up.today, null, Item.PLAIN);  
            //time.setFont(getFont());
        }                         
        return itemtTmeStringItem;
    }
//</editor-fold>    

//<editor-fold defaultstate="collapsed" desc=" time1StringItem ">
    public StringItem time1StringItem() {
        if (time1 == null) {                                 
            // write pre-init user code here
            time1 = new StringItem(null,up.today+"\n", Item.PLAIN);  
            //time1.setFont(getFont());
        }                         
        return time1;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" locationStringItem ">    
    public StringItem locationStringItem() {
        if (locationStringItem == null) {                                 
            // write pre-init user code here
            locationStringItem = new StringItem(null, Strings.emptyString, Item.PLAIN);  
            //locationStringItem.setFont(getFont());
        }                         
        return locationStringItem;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" shiftLocationStringItem ">    
    public StringItem shiftLocationStringItem() {
        if (shiftLocationStringItem == null) {                                 
            // write pre-init user code here
            shiftLocationStringItem = new StringItem(Strings.emptyString, Strings.emptyString, Item.PLAIN);  
            //locationStringItem.setFont(getFont());
        }                         
        return shiftLocationStringItem;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" officerStringItem ">    
    private StringItem officerStringItem() {
        if (officerStringItem == null) {                                 
            // write pre-init user code here
            officerStringItem = new StringItem(null,Strings.emptyString, Item.PLAIN);  
            //officerStringItem.setFont(getFont());
        }                         
        return officerStringItem;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" phoneStringItem ">    
    public StringItem phoneStringItem() {
        if (phoneStringItem == null) {                                 
            // write pre-init user code here
            phoneStringItem = new StringItem(null,Strings.emptyString, Item.PLAIN);  
            //phoneStringItem.setFont(getFont());
        }                         
        return phoneStringItem;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" idTextField ">                                   
    public TextField idTextField() {
        if (idTextField == null) {                                 
            idTextField = new TextField("Enter Guard ID",Strings.emptyString, 6, TextField.NUMERIC);
        }                         
        return idTextField;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" imeTextField ">                                   
    public TextField imeiTextField() {
        if (imeiTextField == null) {                                 
            imeiTextField = new TextField("Enter Phone IMEI",Strings.emptyString,15, TextField.NUMERIC);
        }                         
        return imeiTextField;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="connURLTextField">                                   
    public TextField connURLTextField() {
        if (connURLTextField == null) {                                 
            connURLTextField = new TextField("Enter Connection URL",Strings.hntURL,100, TextField.ANY);
        }                         
        return connURLTextField;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" attDescription ">                                   
    public TextField attDescription() {
        if (attDescription == null) {                                 
            attDescription = new TextField("Descrption",Strings.emptyString, 160, TextField.ANY);
            //idTextField.setPreferredSize(100, 30);
        }                         
        return attDescription;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" descriptionTextField ">
    public TextField descriptionTextField() {
        if (descriptionTextField == null) {                                 
            descriptionTextField = new TextField("Description",Strings.emptyString, 160, TextField.ANY);
            descriptionTextField.setMaxSize(160);
            //idTextField.setPreferredSize(100, 30);
        }                         
        return descriptionTextField;
    }
//</editor-fold> 
    
//<editor-fold defaultstate="collapsed" desc=" checkPointIDTextField ">
    private TextField checkPointIDTextField() {
        if (checkPointIDTextField == null) {                                 
            checkPointIDTextField = new TextField("Enter Check Point ID",Strings.emptyString, 6, TextField.NUMERIC);
            //idTextField.setPreferredSize(100, 30);
        }                         
        return checkPointIDTextField;
    }
//</editor-fold>     
    
//<editor-fold defaultstate="collapsed" desc=" searchTextField ">    
    public TextField searchTextField() {
        if (searchTextField == null) {                                 
            searchTextField = new TextField("Select Site",Strings.emptyString, 10, TextField.ANY);
            searchTextField.setItemCommandListener(this);
        }                         
        return searchTextField;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" shiftsTextField ">    
    public TextField shiftsTextField() {
        if (shiftsTextField == null) {                                 
            shiftsTextField = new TextField("Enter Exp Shifts",Strings.emptyString, 3, TextField.NUMERIC);
        }                         
        return shiftsTextField;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" adjTextField ">    
    public TextField adjTextField() {
        if (adjTextField == null) {                                 
            adjTextField = new TextField("Adj.",Strings.emptyString, 3, TextField.NUMERIC);
        }                         
        return adjTextField;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" itemNameTextField ">    
    public TextField itemNameTextField() {
        if (itemNameTextField == null) {                                 
            itemNameTextField = new TextField("Search",Strings.emptyString, 5, TextField.ANY);
            itemNameTextField.setItemCommandListener(this);
        }                         
        return itemNameTextField;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" itemQtyTextField ">    
    public TextBox itemQtyTextField() {
        if (itemQtyTextField == null) {                                 
            itemQtyTextField = new TextBox("Item Quantity",Strings.emptyString, 5, TextField.NUMERIC);
            itemQtyTextField.addCommand(okCommand());
            itemQtyTextField.addCommand(getBackCommand());
            itemQtyTextField.setCommandListener(this);
        }                         
        return itemQtyTextField;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="actionsForm">
    public Form actionsForm() {
        if (actionsForm == null) {
            actionsForm  = new Form("Attendance", new Item[]{actionsCheckPointIDTextBox(),staffIDTextBox(),actionsChoiceGroup(),attDescription()});
            actionsForm.addCommand(cancelCommand());
            actionsForm.addCommand(getBackCommand());
            actionsForm.addCommand(submitCommand());
            actionsForm.setCommandListener(this);
        }
        return actionsForm;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="pinOtpForm">
    public Form pinOtpForm() {
        if (pinOtpForm == null) {
            pinOtpForm  = new Form("Attendance", new Item[]{actionsPINTextBox(),actionsOTPTextBox()});
            pinOtpForm.addCommand(cancelCommand());
            pinOtpForm.addCommand(getBackCommand());
            pinOtpForm.addCommand(submitCommand());
            pinOtpForm.setCommandListener(this);
        }
        return pinOtpForm;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" idEntryForm ">
    public Form idEntryForm() {
        if (idEntryForm == null) {
            // write pre-init user code here
            idEntryForm  = new Form("Attendance", new Item[]{timeStringItem(),shiftLocationStringItem(),shiftChoiceGroup(),idTextField()});
            idEntryForm .addCommand(cancelCommand());
            idEntryForm .addCommand(getBackCommand());
            idEntryForm .addCommand(submitCommand());
            idEntryForm .setCommandListener(this);
            // write post-init user code here
        }
        return idEntryForm ;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" clockIncidentForm ">
    public Form clockIncidentForm() {
        if (clockIncidentForm == null) {
            // write pre-init user code here
            clockIncidentForm  = new Form("Supervisor Tour", new Item[]{clockTimeStringItem(),clocksChoiceGroup(),descriptionTextField()});
            clockIncidentForm .addCommand(cancelCommand());
            clockIncidentForm .addCommand(getBackCommand());
            clockIncidentForm .addCommand(submitCommand());
            clockIncidentForm .setCommandListener(this);
            // write post-init user code here
        }
        return clockIncidentForm ;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" checkPointForm ">
    private Form checkPointForm() {
        if (checkPointForm == null) {
            // write pre-init user code here
            checkPointForm  = new Form("Supervisor Tour", new Item[]{timeStringItem2(),checkPointIDTextField()});
            checkPointForm .addCommand(cancelCommand());
            checkPointForm .addCommand(getBackCommand());
            checkPointForm .addCommand(submitCommand());
            checkPointForm .setCommandListener(this);
            // write post-init user code here
        }
        return checkPointForm ;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" commitForm">
    public Form commitForm() {
        if (commitForm == null) {
            commitForm  = new Form("Attendance", new Item[]{commitStringItem(),commitStringItem2(),commitStringItem3()});
            commitForm.addCommand(cancelCommand());
            commitForm.addCommand(commitCommand());
            commitForm.setCommandListener(this);
        }
        return commitForm;
    }
//</editor-fold>    

//<editor-fold defaultstate="collapsed" desc=" confirmShiftsForm ">
    public Form confirmShiftsForm() {
        if (confirmShiftsForm == null) {
            // write pre-init user code here
            confirmShiftsForm  = new Form(Strings.formsTitle, new Item[]{});
            confirmShiftsForm .addCommand(cancelCommand());
            confirmShiftsForm.addCommand(getBackCommand());
            confirmShiftsForm.addCommand(submitCommand());
            confirmShiftsForm.setCommandListener(this);
            // write post-init user code here
        }
        return confirmShiftsForm ;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" tourConfirmForm ">
    public Form tourConfirmForm() {
        if (tourConfirmForm == null) {
            // write pre-init user code here
            tourConfirmForm  = new Form("Supervisor Tour", new Item[]{});
            tourConfirmForm .addCommand(cancelCommand());
            tourConfirmForm.addCommand(getBackCommand());
            tourConfirmForm.addCommand(submitCommand());
            tourConfirmForm.setCommandListener(this);
            // write post-init user code here
        }
        return tourConfirmForm ;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" ShiftsSuccessForm ">
    public Form shiftsSuccessForm() {
        if (shiftsSuccessForm == null) {
            // write pre-init user code here
            shiftsSuccessForm  = new Form("Schedule Shifts", new Item[]{});
            //shiftsSuccessForm.addCommand(newSubmissionCommand());
            shiftsSuccessForm.addCommand(mainMenuCommand());
            shiftsSuccessForm.setCommandListener(this);//
            // write post-init user code here
        }
        return shiftsSuccessForm;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" tourSuccessForm ">
    public Form tourSuccessForm() {
        if (tourSuccessForm == null) {
            // write pre-init user code here
            tourSuccessForm  = new Form("Supervisor Tour", new Item[]{});
            //tourSuccessForm.addCommand(newSubmissionCommand());
            tourSuccessForm.addCommand(mainMenuCommand());
            tourSuccessForm.setCommandListener(this);
            // write post-init user code here
        }
        return tourSuccessForm;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" itemSuccessForm ">
    public Form itemSuccessForm() {
        if (itemSuccessForm == null) {
            // write pre-init user code here
            itemSuccessForm  = new Form("Item Submission", new Item[]{});
            //itemSuccessForm.addCommand(newSubmissionCommand());
            itemSuccessForm.addCommand(mainMenuCommand());
            itemSuccessForm.setCommandListener(this);
            // write post-init user code here
        }
        return itemSuccessForm;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" createItemSuccessForm"> 
    public void createItemSuccessForm(String date,String confNumber,String location,String item,String unit,String qty,String status){
       //this form is used to view pending, history and successful transactions
        try{
            itemSuccessForm().deleteAll();
            itemSuccessForm().append(new StringItem(date+"\n",null));
            //server connections fail
            //System.out.println(connectionIsAvailable+"\n"+hisPendFlag);
            if((connectionIsAvailable == false&&hisPendFlag==false)/*||(hisPendFlag == true&&historyFlag == false)*/){
                itemSuccessForm().append(new StringItem(null,"Transaction Incomplete, saved for later submission\n"));
                itemSuccessForm().append(new StringItem("Transaction Number: ",confNumber+"\n"));
            }
            //server connections successful
            else if(connectionIsAvailable == true&&hisPendFlag==false){
                itemSuccessForm().append(new StringItem(null,"Transaction Successful"+"\n"));
                itemSuccessForm().append(new StringItem("Confirmation Number: ",confNumber+"\n"));
            }
            //coming from pending or history transactions
            else{
                if(pendingFlag==true){
                    itemSuccessForm().append(new StringItem(null,"Transaction Incomplete\n"));
                    itemSuccessForm().append(new StringItem("Transaction Number: ",confNumber+"\n"));
                }else{
                    itemSuccessForm().append(new StringItem("Status: ",status+"\n"));
                    itemSuccessForm().append(new StringItem("Confirmation Number: ",confNumber+"\n"));
                }
            }
            //generic information on successForm
            itemSuccessForm().append(new StringItem("Item: ",item+"\n"));
            //itemSuccessForm().append(new StringItem(null,"Item Units: "+unit+"\n"));
            itemSuccessForm().append(new StringItem("Qty: ",qty+unit+"\n"));
            itemSuccessForm().append(new StringItem("Site: ",location+"\n"));
            //avoid saving transaction when from history or pending
            if(hisPendFlag==false){
                //save after successfuly connectining to server
                saveTransaction(activeTrxn.toString());
                itemSuccessForm().removeCommand(getBackCommand());
                itemSuccessForm().addCommand(newSubmissionCommand());
            }
            else{
                itemSuccessForm().removeCommand(newSubmissionCommand());
                itemSuccessForm().addCommand(getBackCommand());
            }
            switchDisplayable(null,itemSuccessForm());
        }
        catch(Exception ex){
            showAlert(Strings.errorAlertTitle,ex.getMessage());
        } 
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" createShiftsSuccessForm "> 
    public void createShiftsSuccessForm(String date,String confNumber,String location,String officer,String phone,String expShifts,String status){
       //this form is used to view pending, history and successful transactions
        try{
            shiftsSuccessForm().deleteAll();
            shiftsSuccessForm().append(new StringItem(date,null));
            //server connections fail
            if((connectionIsAvailable == false&&hisPendFlag==false)/*||(hisPendFlag == true&&historyFlag == false)*/){
                shiftsSuccessForm().append(new StringItem("Transaction Incomplete, saved for later submission",null));
            }
            //server connections successful
            else if(connectionIsAvailable == true&&hisPendFlag==false){
                shiftsSuccessForm().append(new StringItem("Transaction Successful",null));
                shiftsSuccessForm().append(new StringItem("Confirmation Number: "+confNumber,null));
            }
            //coming from pending or history transactions
            else{
                if(pendingFlag==true){
                    shiftsSuccessForm().append(new StringItem("Transaction Incomplete",null));
                    shiftsSuccessForm().append(new StringItem("Trxn Number: "+confNumber,null));
                }else{
                    shiftsSuccessForm().append(new StringItem("Status: "+status,null));
                    shiftsSuccessForm().append(new StringItem("Confirmation Number: "+confNumber,null));
                }
            }
            //generic information on successForm
            shiftsSuccessForm().append(new StringItem("SITE: ",location+"\n"));
            shiftsSuccessForm().append(new StringItem("OFFICER: ",officer+"\n"));
            //shiftsSuccessForm().append(new StringItem("NUMBER: ",phone+"\n"));
            shiftsSuccessForm().append(new StringItem("EXP. SHIFTS: ",expShifts+"\n"));
            //avoid saving transaction when from history or pending
            if(hisPendFlag==false){
                //save after successfuly connectining to server
                saveTransaction(activeTrxn.toString());
                shiftsSuccessForm().removeCommand(getBackCommand());
                shiftsSuccessForm().addCommand(newSubmissionCommand());
            }
            else{
                shiftsSuccessForm().removeCommand(newSubmissionCommand());
                shiftsSuccessForm().addCommand(getBackCommand());
            }
            switchDisplayable(null,shiftsSuccessForm());
        }
        catch(Exception ex){
            showAlert(Strings.errorAlertTitle,ex.getMessage());
        } 
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" createTourSuccessForm"> 
    public void createTourSuccessForm(String date,String confNumber,String location,String action,String descrip,String status){
       //this form is used to view pending, history and successful transactions
        try{
            tourSuccessForm().deleteAll();
            tourSuccessForm().append(new StringItem(date,null));
            //server connections fail
            if((connectionIsAvailable == false&&hisPendFlag==false)/*||(hisPendFlag == true&&historyFlag == false)*/){
                tourSuccessForm().append(new StringItem("Transaction Incomplete, saved for later submission",null));
            }
            //server connections successful
            else if(connectionIsAvailable == true&&hisPendFlag==false){
                tourSuccessForm().append(new StringItem("Transaction Successful",null));
                tourSuccessForm().append(new StringItem("Confirmation Number: "+confNumber,null));
            }
            //coming from pending or history transactions
            else{
                if(pendingFlag==true){
                    tourSuccessForm().append(new StringItem("Transaction Incomplete",null));
                    tourSuccessForm().append(new StringItem("Trxn Number: "+confNumber,null));
                }else{
                    tourSuccessForm().append(new StringItem("Status: "+status,null));
                    tourSuccessForm().append(new StringItem("Confirmation Number: "+confNumber,null));
                }
            }
            //generic information on successForm
            tourSuccessForm().append(new StringItem("Check Point: "+location,null));
            tourSuccessForm().append(new StringItem("Action: "+action,null));
            tourSuccessForm().append(new StringItem("Descr: "+descrip,null));
            //tourSuccessForm().append(new StringItem(null,"Name: "+nemu+"\n"));
            
            //avoid saving transaction when from history or pending
            if(hisPendFlag==false){
                //save after successfuly connectining to server
                saveTransaction(activeTrxn.toString());
                tourSuccessForm().removeCommand(getBackCommand());
                tourSuccessForm().addCommand(newSubmissionCommand());
            }
            else{
                tourSuccessForm().removeCommand(newSubmissionCommand());
                tourSuccessForm().addCommand(getBackCommand());
            }
            switchDisplayable(null,tourSuccessForm());
        }
        catch(Exception ex){
            showAlert(Strings.errorAlertTitle,ex.getMessage());
        } 
    }
//</editor-fold>
 
//<editor-fold defaultstate="collapsed" desc=" setConfirmShiftsForm ">    
    private void setConfirmShiftsForm(){
        confirmShiftsForm().deleteAll();
        expShifts = shiftsTextField().getString();
        adj = adjTextField().getString();
        if(validateFields("Adj & ExpShifts are required!",expShifts)==true&&validateFields("Adj & ExpShifts are required!", adj)==true){
            //shiftsTextField.setString("");
            confirmShiftsForm.setTitle("Confirm Details");
            confirmShiftsForm.append(new StringItem(up.today,null, Item.PLAIN));
            confirmShiftsForm.append(new StringItem("SITE: ",selectedLocation+"\n", Item.PLAIN));
            confirmShiftsForm.append(new StringItem("OFFICER: ",officer+"\n", Item.PLAIN));
            confirmShiftsForm.append(new StringItem("NUMBER: ",phoneNum+"\n", Item.PLAIN));
            confirmShiftsForm.append(new StringItem("EXP. SHIFTS: ",expShifts+"\n", Item.PLAIN));
            switchDisplayable(null, confirmShiftsForm);
        }
    }
//</editor-fold>   
    
//<editor-fold defaultstate="collapsed" desc=" createTourConfirmForm ">    
    private void createTourConfirmForm(){
        tourConfirmForm().deleteAll();
        tourConfirmForm().setTitle("Confirm Details");
        selectedAction = clocksChoiceGroup.getString(clocksChoiceGroup.getSelectedIndex());
        description = descriptionTextField.getString();
        if(selectedAction.equals("INCIDENT")){
            if(description.equals(Strings.emptyString)){
                showAlert("Description Error","Incident missing Description!");
            }
            else{
                //tourConfirmForm.append(new StringItem(null, "Please Confirm Details\n", Item.PLAIN));
                tourConfirmForm.append(new StringItem(up.today,null, Item.PLAIN));
                tourConfirmForm.append(new StringItem("Check Point: "+checkPointID,null, Item.PLAIN));
                tourConfirmForm.append(new StringItem("Action: "+selectedAction,null, Item.PLAIN));
                tourConfirmForm.append(new StringItem("Descr: "+description,null, Item.PLAIN));
                //tourConfirmForm.append(new StringItem(null, "Name: "+name+"\n", Item.PLAIN));
                //tourConfirmForm.append(new StringItem(null, "Guard ID: "+id+"\n", Item.PLAIN));
                switchDisplayable(null, tourConfirmForm);
            }
        }
        else{
            description = "n/a";
            //tourConfirmForm.append(new StringItem(null, "Please Confirm Details\n", Item.PLAIN));
            tourConfirmForm.append(new StringItem(up.today,null, Item.PLAIN));
            tourConfirmForm.append(new StringItem("Check Point: "+checkPointID,null, Item.PLAIN));
            tourConfirmForm.append(new StringItem("Action: "+selectedAction,null, Item.PLAIN));
            //tourConfirmForm.append(new StringItem(null, "Descr: "+description+"\n", Item.PLAIN));
            //tourConfirmForm.append(new StringItem(null, "Name: "+name+"\n", Item.PLAIN));
            //tourConfirmForm.append(new StringItem(null, "Guard ID: "+id+"\n", Item.PLAIN));
            switchDisplayable(null, tourConfirmForm);
        }
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" createItemConfirmForm ">    
    private void createItemConfirmForm(){
        confirmItemForm().deleteAll();
        confirmItemForm().setTitle("Confirm Details");
        //itemUnit = itemUnitsChoiceGroup.getString(itemUnitsChoiceGroup.getSelectedIndex());
        itemQty = itemQtyTextField.getString();
        itemNameTextField.setString(Strings.emptyString);
        if(itemQty.equals(Strings.emptyString)){
            showAlert("Field Error","Quantity is required!");
        }
        else{
            //confirmItemForm.append(new StringItem(null, "Please Confirm Details\n", Item.PLAIN));
            confirmItemForm.append(new StringItem(up.today+"\n",null, Item.PLAIN));
            //confirmItemForm.append(new StringItem(null, "Item ID: "+itemID+"\n", Item.PLAIN));
            confirmItemForm.append(new StringItem("Item Name: ", name+"\n", Item.PLAIN));
            //confirmItemForm.append(new StringItem(null, "Item Units: "+itemUnit+"\n", Item.PLAIN));
            confirmItemForm.append(new StringItem("QTY: ",itemQty+" "+itemUnit+"\n", Item.PLAIN));
            confirmItemForm.append(new StringItem( "Site: ",selectedLocation+"\n", Item.PLAIN));
            switchDisplayable(null, confirmItemForm);
        }
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" expectedShiftsForm ">
    public Form expectedShiftsForm() {
        if (expectedShiftsForm == null) {
            // write pre-init user code here
            expectedShiftsForm  = new Form(Strings.formsTitle, new Item[]{time1StringItem(),locationStringItem(),officerStringItem(),phoneStringItem(),shiftsTextField(),adjTextField()});
            expectedShiftsForm .addCommand(cancelCommand());
            expectedShiftsForm.addCommand(getBackCommand());
            expectedShiftsForm.addCommand(submitCommand());
            expectedShiftsForm.setCommandListener(this);
            // write post-init user code here
        }
        return expectedShiftsForm ;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" setExpectedShiftsForm ">    
    public void setExpectedShiftsForm(){
        try{
            selectedLocation = locationsChoiceGroup().getString(locationsChoiceGroup.getSelectedIndex());
            selectedLocationID = locationANDid.get(selectedLocation).toString();
            expectedShiftsForm().setTitle("Schedule Shifts");
            Vector officerVector = split(up.locationsHashTable.get(selectedLocationID).toString(),"#");
            officer = officerVector.elementAt(0).toString();
            phoneNum = officerVector.elementAt(2).toString();
            locationStringItem.setText(selectedLocation+"\n");
            locationStringItem.setLabel("Site: ");
            officerStringItem.setText(officer+"\n");
            officerStringItem.setLabel("Officer: ");
            phoneStringItem.setLabel("PhoneNum: ");
            phoneStringItem.setText(phoneNum+"\n");
            switchDisplayable(null, expectedShiftsForm);
        }
        catch(Exception ex){
            getAlert().setTitle(Strings.errorAlertTitle);
            getAlert().setString(ex.getMessage());
            switchDisplayable(null,getAlert());
        }
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" shiftsSubmission_locationsForm ">    
    public Form shiftSubmission_locationsForm() {
        if (shiftSubmission_locationsForm == null) {
            // write pre-init user code here
            shiftSubmission_locationsForm  = new Form("Attendance", new Item[]{});
            shiftSubmission_locationsForm .addCommand(cancelCommand());
            shiftSubmission_locationsForm .addCommand(getBackCommand());
            shiftSubmission_locationsForm .addCommand(submitCommand());
            shiftSubmission_locationsForm.setItemStateListener(this);
            shiftSubmission_locationsForm .setCommandListener(this);
            // write post-init user code here
        }
        return shiftSubmission_locationsForm ;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" storelocationsForm ">    
    public Form storelocationsForm() {
        if (storelocationsForm == null) {
            // write pre-init user code here
            storelocationsForm  = new Form("Item Submission", new Item[]{});
            storelocationsForm .addCommand(cancelCommand());
            storelocationsForm .addCommand(getBackCommand());
            storelocationsForm .addCommand(submitCommand());
            storelocationsForm.setItemStateListener(this);
            storelocationsForm .setCommandListener(this);
            // write post-init user code here
        }
        return storelocationsForm ;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" scheduleShiftLocationsForm ">    
    public Form scheduleShiftLocationsForm() {
        if (scheduleShiftLocationsForm == null) {
            // write pre-init user code here
            scheduleShiftLocationsForm  = new Form("Schedule Shifts", new Item[]{});
            scheduleShiftLocationsForm .addCommand(cancelCommand());
            scheduleShiftLocationsForm .addCommand(getBackCommand());
            scheduleShiftLocationsForm .addCommand(submitCommand());
            scheduleShiftLocationsForm.setItemStateListener(this);
            scheduleShiftLocationsForm .setCommandListener(this);
            // write post-init user code here
        }
        return scheduleShiftLocationsForm ;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" itemsForm ">    
    public Form itemsForm() {
        if (itemsForm == null) {
            // write pre-init user code here
            itemsForm  = new Form("Select Item", new Item[]{itemNameTextField(),itemsChoiceGroup()});
            itemsForm.addCommand(cancelCommand());
            itemsForm.addCommand(getBackCommand());
            itemsForm.addCommand(submitCommand());
            itemsForm.setItemStateListener(this);
            itemsForm.setCommandListener(this);
            // write post-init user code here
        }
        return itemsForm ;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" passWordForm ">
    /**
     * Returns an initialized instance of form component.
     *
     * @return the initialized component instance
     */
    public Form passWordForm() {
        if (passWordForm == null) {
            // write pre-init user code here
            passWordForm  = new Form("Save Password", new Item[]{new TextField("Enter Password",Strings.emptyString, 20, TextField.PASSWORD),new TextField("Confirm Password",Strings.emptyString, 20, TextField.PASSWORD)});
            passWordForm .addCommand(getExitCommand());
            passWordForm .addCommand(saveCommand());
            passWordForm .setCommandListener(this);
            // write post-init user code here
        }
        return passWordForm ;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" confirmDetailsForm ">
    /**
     * Returns an initialized instance of confirmDetails component.
     *
     * @return the initialized component instance
     */
    public Form confirmDetails() {
        if (confirmDetails == null) {
            confirmDetails = new Form(Strings.formsTitle, new Item[]{});
            confirmDetails.addCommand(cancelCommand());
            confirmDetails.addCommand(getBackCommand());
            confirmDetails .addCommand(submitCommand());
            confirmDetails.setCommandListener(this);
        }
        return confirmDetails;
    }
//</editor-fold>
  
//<editor-fold defaultstate="collapsed" desc=" confirmAttendanceForm ">
    public Form confirmAttendanceForm() {
        if (confirmAttendanceForm == null) {
            confirmAttendanceForm = new Form(Strings.formsTitle, new Item[]{});
            confirmAttendanceForm.addCommand(cancelCommand());
            confirmAttendanceForm.addCommand(getBackCommand());
            confirmAttendanceForm .addCommand(submitCommand());
            confirmAttendanceForm.setCommandListener(this);
        }
        return confirmAttendanceForm;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" confirmItemForm ">
    public Form confirmItemForm() {
        if (confirmItemForm == null) {
            confirmItemForm = new Form(Strings.formsTitle, new Item[]{});
            confirmItemForm.addCommand(cancelCommand());
            confirmItemForm.addCommand(getBackCommand());
            confirmItemForm .addCommand(submitCommand());
            confirmItemForm.setCommandListener(this);
        }
        return confirmItemForm;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" login">
    /*private void login(){
        try{
            if(password.getNumRecords()==0){//no password saved previously
                switchDisplayable(null, passWordForm());//enter new password
                password.closeRecordStore();
            }
            else{//pass exists
                resetPasswordFlag = false;
                switchDisplayable(null, passwordTextBox());// login                                            
                password.closeRecordStore();
                // write post-action user code here
            }
        }
        catch(RecordStoreException ex){
            showAlert("Recordstore Error",ex.getMessage());
        }
    }*/
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" waitForm ">
    /**
     * Returns an initialized instance of successWaitForm component.
     *
     * @return the initialized component instance
     */
    public Form waitForm() {
        if (waitForm == null) {
            waitForm = new Form(Strings.formsTitle, new Item[]{});
            waitForm.addCommand(cancelCommand());
            waitForm.setTicker(getTicker());
            waitForm.setCommandListener(this);
        }
        return waitForm;
    }
//</editor-fold> 
    
//<editor-fold defaultstate="collapsed" desc=" incorrectPinForm ">
    /**
     * Returns an initialized instance of incorrectPinForm component.
     *
     * @return the initialized component instance
     */
    public Form incorrectPinForm() {
        if (incorrectPinForm == null) {
            incorrectPinForm = new Form(Strings.formsTitle, new Item[]{});
            incorrectPinForm.addCommand(cancelCommand());
            incorrectPinForm.addCommand(getBackCommand());
            incorrectPinForm.setCommandListener(this);
        }
        return incorrectPinForm;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" supportForm ">
    public TextBox supportForm() {
        if (supportForm == null) {
            supportForm = new TextBox("Technical Support", "Call us on:\n\n0781191914\n0783131266", 200, TextField.ANY | TextField.UNEDITABLE);
            supportForm.addCommand(mainMenuCommand());
            supportForm.setCommandListener(this);
        }
        return supportForm;
    }
    /*public Form supportForm() {
        if (supportForm == null) {
            supportForm = new Form("Technical Support", new Item[]{new StringItem("Call us on:", "\n\n", Item.PLAIN),new StringItem(null, "0781191914", Item.PLAIN),new StringItem(null, "0783131266", Item.PLAIN)});
            supportForm.addCommand(mainMenuCommand());
            supportForm.setCommandListener(this);
        }
        return supportForm;
    }*/
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" applicationsChoiceGroup ">
    public ChoiceGroup applicationsChoiceGroup() {
        if (categoryChoiceGroup == null) {
            categoryChoiceGroup = new ChoiceGroup("Main Menu", Choice.EXCLUSIVE);
            categoryChoiceGroup.append("Attendance", null);
            categoryChoiceGroup.append("Technical Support", null);
            categoryChoiceGroup.setLayout(ImageItem.LAYOUT_DEFAULT);
        }
        return categoryChoiceGroup;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="actionsChoiceGroup">
    public ChoiceGroup actionsChoiceGroup() {
        if (actionsChoiceGroup == null) {
            actionsChoiceGroup = new ChoiceGroup("Actions", Choice.EXCLUSIVE);
            actionsChoiceGroup.append("Presence Confirmed", null);
            actionsChoiceGroup.append("Incident", null);
            actionsChoiceGroup.setLayout(ImageItem.LAYOUT_DEFAULT);
        }
        return actionsChoiceGroup;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" operationsChoiceGroup ">    
    /*public ChoiceGroup operationsChoiceGroup() {
        if (operationsChoiceGroup == null) {
            operationsChoiceGroup = new ChoiceGroup("", Choice.EXCLUSIVE);
            operationsChoiceGroup.append("View History", null);//
            operationsChoiceGroup.append("Pending Submissions", null);
            operationsChoiceGroup.append("Get New App Version", null);
            operationsChoiceGroup.setLayout(ImageItem.LAYOUT_DEFAULT);
            // write post-init user code here
        }
        return operationsChoiceGroup;
    }*/
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" shiftChoiceGroup ">
    public ChoiceGroup shiftChoiceGroup() {
        if (shiftChoiceGroup == null) {
            shiftChoiceGroup = new ChoiceGroup("Shift", Choice.POPUP);
            shiftChoiceGroup.append("DAY", null);
            shiftChoiceGroup.append("NIGHT", null);
            shiftChoiceGroup.append("DAY+NIGHT", null);
            shiftChoiceGroup.append("ABSENT-D", null);
            shiftChoiceGroup.append("ABSENT-N", null);
            shiftChoiceGroup.append("SICK-D", null);
            shiftChoiceGroup.append("SICK-N", null);
            shiftChoiceGroup.append("LEAVE-D", null);
            shiftChoiceGroup.append("LEAVE-N", null);
            shiftChoiceGroup.append("OFF-D", null);
            shiftChoiceGroup.append("OFF-N", null);
            shiftChoiceGroup.setLayout(ImageItem.LAYOUT_DEFAULT);
            //shiftChoiceGroup.setFont(0, getFont());
            //shiftChoiceGroup.setFont(1, getFont());
        }
        return shiftChoiceGroup;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" itemsChoiceGroup ">    
    public ListFilter itemsChoiceGroup() {
        itemsVector = getItems();
        if (itemsChoiceGroup == null) {
            itemsChoiceGroup = new ListFilter(Strings.emptyString, Choice.EXCLUSIVE,itemsVector,this);
        }
        return itemsChoiceGroup;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" clocksChoiceGroup ">    
    public ChoiceGroup clocksChoiceGroup() {
        if (clocksChoiceGroup == null) {
            clocksChoiceGroup = new ChoiceGroup(Strings.emptyString, Choice.EXCLUSIVE);
            clocksChoiceGroup.append("CLOCK IN", null);
            clocksChoiceGroup.append("CLOCK OUT", null);
            clocksChoiceGroup.append("INCIDENT", null);
            clocksChoiceGroup.setLayout(ImageItem.LAYOUT_DEFAULT);
        }
        return clocksChoiceGroup;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" locationsChoiceGroup ">
    public ListFilter locationsChoiceGroup(){
        if(locationsChoiceGroup == null){
            locationsChoiceGroup = new ListFilter(Strings.emptyString, Choice.EXCLUSIVE,getLocations(),this);
        } 
        return locationsChoiceGroup;
    }
    //</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" historyPendingForm ">
    /**
     * Returns an initialized instance of historyPendingForm component.
     *
     * @return the initialized component instance
     */
    public Form historyPendingForm() {
        if (historyPendingForm == null) {
            historyPendingForm = new Form(Strings.formsTitle, new Item[]{historyPendingChoiceGroup()});
            historyPendingForm.addCommand(cancelCommand());
            historyPendingForm.addCommand(mainMenuCommand());
            historyPendingForm.addCommand(viewCommand());
            historyPendingForm.setCommandListener(this);
        }
        return historyPendingForm;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" historyPendingChoiceGroup ">
    /**
     * Returns an initialized instance of historyPendingChoiceGroup component.
     *
     * @return the initialized component instance
     */
    public ChoiceGroup historyPendingChoiceGroup() {
        if (historyPendingChoiceGroup == null) {
            // write pre-init user code here
            historyPendingChoiceGroup = new ChoiceGroup("Pending Transactions", Choice.EXCLUSIVE);
            historyPendingChoiceGroup.setLayout(ImageItem.LAYOUT_DEFAULT);
            // write post-init user code here
        }
        return historyPendingChoiceGroup;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" splashScreen ">
    public SplashScreen getSplashScreen() {
        if (splashScreen == null) {
            // write pre-init user code here
            splashScreen = new SplashScreen(getDisplay());
            splashScreen.setTitle(Strings.formsTitle);
            //splashScreen.setTicker(getTicker());
            splashScreen.setCommandListener(this);
            splashScreen.setImage(iconImage());
            //splashScreen.setText("powered by Peak Bandwidth");
            splashScreen.setTextFont(getFont());
            splashScreen.setTimeout(5000);
            // write post-init user code here
        }
        return splashScreen;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" iconImage ">
    public Image iconImage() {
        if (iconImage == null) {
            try {
                iconImage = Image.createImage(Strings.image);
            } catch (java.io.IOException e) {
                getAlert().setType(AlertType.ERROR);
                getAlert().setTitle(Strings.errorAlertTitle);
                getAlert().setString(e.getMessage());
                switchDisplayable(null,getAlert());
            }
        }
        return iconImage;
    }
    //</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" ticker ">
    /**
     * Returns an initialized instance of ticker component.
     *
     * @return the initialized component instance
     */
    private Ticker getTicker() {
        if (ticker == null) {
            // write pre-init user code here
            ticker = new Ticker(".....checking for updates....");
            // write post-init user code here
        }
        return ticker;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" alert ">
    public Alert getAlert() {
        if (alert == null) {
            alert = new Alert(Strings.errorAlertTitle, "error!", null, AlertType.ERROR);
            alert.setTimeout(3000);
        }
        return alert;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" timeOutAlert ">
    public Alert timeOutAlert() {
        if (timeOutAlert == null) {
            timeOutAlert = new Alert("Timeout", "Connection timeout! Do you want to try again?", null, AlertType.INFO);
            timeOutAlert.setTimeout(Alert.FOREVER);
            timeOutAlert.addCommand(okCommand());
            timeOutAlert.addCommand(cancelCommand());
            timeOutAlert.setCommandListener(this);
        }
        return timeOutAlert;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" split">
    public static Vector split(String splitStr, String delimiter) {
        //String [] splitArray;
            StringBuffer token = new StringBuffer();
            Vector tokens = new Vector();

            // split
            char[] chars = splitStr.toCharArray();
            for (int i=0; i < chars.length; i++) {
                if (delimiter.indexOf(chars[i]) != -1) {
                    // we bumbed into a delimiter
                    if (token.length() > 0) {
                        tokens.addElement(token.toString());
                        token.setLength(0);
                    }
                }
                else {
                    token.append(chars[i]);
                }
            }
            // don't forget the "tail"...
            if (token.length() > 0) {
                tokens.addElement(token.toString());
            }
            return tokens;
        }
    //</editor-fold>    

//<editor-fold defaultstate="collapsed" desc=" mainMenuSubmitAction">    
private void mainMenuSubmitAction(){
    state = mainMenu();
    //searchTextField(),locationsChoiceGroup()
    try{ 
        selectedOperation = categoryChoiceGroup.getString(categoryChoiceGroup.getSelectedIndex());
        if(selectedOperation.equalsIgnoreCase("Attendance")){
            //locationsForm().setTitle("Guard Manager");
            resetLocationsForms();
            shiftSubmission_locationsForm().append(searchTextField());
            searchTextField().setString(Strings.emptyString);
            shiftSubmission_locationsForm().append(locationsChoiceGroup());
            switchDisplayable(null, shiftSubmission_locationsForm());
        }
        else if(selectedOperation.equalsIgnoreCase("Guard Tour")){
            actionsCheckPointIDTextBox().setString("");
            staffIDTextBox().setString("");
            switchDisplayable(null, actionsForm());
        }
        else if(selectedOperation.equalsIgnoreCase("Schedule Shifts")){
            //state = "Schedule Shifts";
            switchDisplayable(null, scheduleShiftPasswordTextBox());
        }
        else if(selectedOperation.equalsIgnoreCase("Supervisor Tour")){
            //state = "Supervisor Tour";
            checkPointIDTextField().setString(Strings.emptyString);
            switchDisplayable(null, checkPointForm());
        }
        else if(selectedOperation.equalsIgnoreCase("Store Manager")){
            //state = "Store Manager";
            resetLocationsForms();
            storelocationsForm().append(searchTextField());
            searchTextField().setString(Strings.emptyString);
            storelocationsForm().append(locationsChoiceGroup());
            switchDisplayable(null, storelocationsForm());
            //locationsForm().setTitle("iWMS Store Manager");
        }
        else if(selectedOperation.equalsIgnoreCase("Technical Support")){
            switchDisplayable(null, supportForm());
        }
    }
    catch(IndexOutOfBoundsException ex){
        showAlert("Config Error","configuratins are missing!");
    }
    catch(Exception ex){
        showAlert(Strings.errorAlertTitle,ex.getMessage());
    }
}    
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" resetLocationsForms">
private void resetLocationsForms(){
    shiftSubmission_locationsForm().deleteAll();
    storelocationsForm().deleteAll();
    scheduleShiftLocationsForm().deleteAll();
}
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" createConfirmationForm">
    private void createConfirmationForm(){
        confirmDetails().deleteAll();
        confirmDetails().setTitle("Confirm Details");
        pinFlag = false;
            id = ((TextField)idEntryForm().get(3)).getString();
        if(validateFields("Guard ID required!",id)==true){//wether passed validation
            try{
                shift = shiftChoiceGroup.getString(shiftChoiceGroup.getSelectedIndex());
                location = selectedLocation;
                if(!(up.guardHashtable.get(id)==null)){
                    //id returns a string {name_phoneNum_location}
                    Vector guardDetails = split(up.guardHashtable.get(id).toString(),"_");//split returned string by _
                    number = guardDetails.elementAt(1).toString();
                    name = guardDetails.elementAt(0).toString();
                    
                    confirmDetails().append(new StringItem(up.today,null));
                    confirmDetails().append(new StringItem("Attendance",null));
                    confirmDetails().append(new StringItem("Guard ID: "+id,null));
                    confirmDetails().append(new StringItem("Shift: "+shift,null));
                    confirmDetails().append(new StringItem("Name: "+name,null));
                    //confirmDetails().append(new StringItem("NUMBER: ",number+"\n"));
                    confirmDetails().append(new StringItem("Site: "+selectedLocation,null));
                    switchDisplayable(null,confirmDetails());
                }
                else{
                    name = "Guard";
                    number = "Unknown";
                    //confirmDetails().append(new StringItem("Please Confirm Details:\n",null));
                    confirmDetails().append(new StringItem(up.today,null));
                    confirmDetails().append(new StringItem("Attendance",null));
                    confirmDetails().append(new StringItem("Guard ID: "+id,null));
                    confirmDetails().append(new StringItem("Shift: "+shift,null));
                    confirmDetails().append(new StringItem("Name: "+name,null));
                    //confirmDetails().append(new StringItem("NUMBER: ",number+"\n"));
                    confirmDetails().append(new StringItem("SITE: "+selectedLocation,null));
                    
                    getAlert().setType(AlertType.ERROR);
                    getAlert().setTitle("Warning!");
                    getAlert().setString("Guard ID: '"+id+"' not currently in database, submit request to update it.");
                    switchDisplayable(getAlert(),confirmDetails());
                }
            }
            catch(Exception ex){
                //ex.printStackTrace();
                showAlert(Strings.errorAlertTitle,ex.getMessage());
            }
        } 
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" createAttendanceConfirmation">
    private void createAttendanceConfirmation(){
        confirmAttendanceForm().deleteAll();
        confirmAttendanceForm.setTitle("Confirm Details");
        pinFlag = false;
        if(selectedAction.equals("Incident")&&description.equals("")){
            showAlert("Description Error","Incident missing Description!");
        }
        else{
            if(!selectedAction.equals("Incident")){
                description = "n/a";
            }
            //if(validateFields("Guard ID required!",id)==true){//wether passed validation
            try{
                //shift = actionsChoiceGroup().getString(actionsChoiceGroup.getSelectedIndex());
                //location = selectedLocation;
                if(!(up.guardHashtable.get(id)==null)){
                    //id returns a string {name_phoneNum_location}
                    Vector guardDetails = split(up.guardHashtable.get(id).toString(),"_");//split returned string by _
                    number = guardDetails.elementAt(1).toString();
                    name = guardDetails.elementAt(0).toString();
                    //location = guardDetails.elementAt(2).toString();
                    //confirmDetails().append(new StringItem("Please Confirm Details:\n",null));
                    confirmAttendanceForm.append(new StringItem(up.today,null));
                    //confirmDetails().append(new StringItem("Shift Submission\n",null));
                    confirmAttendanceForm.append(new StringItem("Guard ID: "+id,null));
                    confirmAttendanceForm.append(new StringItem("Action: "+selectedAction,null));
                    confirmAttendanceForm.append(new StringItem("Descr: "+description,null, Item.PLAIN));
                    confirmAttendanceForm.append(new StringItem("Name: "+name,null));
                    //confirmAttendanceForm.append(new StringItem("Number: "+number,null));
                    confirmAttendanceForm.append(new StringItem("Site: "+checkPointID,null));
                    switchDisplayable(null,confirmAttendanceForm());
                }
                else{
                    name = "Guard";
                    number = "Unknown";
                    //confirmDetails().append(new StringItem("Please Confirm Details:\n",null));
                    confirmAttendanceForm.append(new StringItem(up.today,null));
                    //confirmDetails().append(new StringItem("Shift Submission\n",null));
                    confirmAttendanceForm.append(new StringItem("Guard ID: "+id,null));
                    confirmAttendanceForm.append(new StringItem("Action: "+selectedAction,null));
                    confirmAttendanceForm.append(new StringItem("Descr: "+description,null, Item.PLAIN));
                    confirmAttendanceForm.append(new StringItem("Name: "+name,null));
                    //confirmAttendanceForm.append(new StringItem("NUMBER: ",number+"\n"));
                    confirmAttendanceForm.append(new StringItem("Site: "+checkPointID,null));
                    
                    getAlert().setType(AlertType.ERROR);
                    getAlert().setTitle("Warning!");
                    getAlert().setString("Guard ID: '"+id+"' not currently in database, submit request to update it.");
                    switchDisplayable(getAlert(),confirmAttendanceForm());
                    
            
                }
            }
            catch(Exception ex){
                ex.printStackTrace();
                showAlert("Error",ex.getMessage());
            }
        } 
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" createWaitForm">
    public void createWaitForm(String ticker){
        waitForm().deleteAll();
        getTicker().setString(ticker);
        waitForm().append(new StringItem("please wait ......",null));
        switchDisplayable(null,waitForm());        
    }    
//</editor-fold>
  
//<editor-fold defaultstate="collapsed" desc=" createSuccessForm">
    public void createSuccessForm(String guardID,String shift,String name,String number,String confNumber,String date,String location,String status){
        //this form is used to view pending, history and successful transactions
        try{
            successForm().deleteAll();
            successForm().append(new StringItem(date,null));
            //server connections fail
            if((connectionIsAvailable == false&&hisPendFlag==false)/*||(hisPendFlag == true&&historyFlag == false)*/){
                successForm().append(new StringItem("Transaction Incomplete, saved for later submission",null));
            }
            //server connections successful
            else if(connectionIsAvailable == true&&hisPendFlag==false){
                successForm().append(new StringItem("Transaction Successful",null));
                successForm().append(new StringItem("Confirmation Number: "+confNumber,null));
            }
            //coming from pending or history transactions
            else{
                if(pendingFlag==true){
                    successForm().append(new StringItem("Transaction Incomplete",null));
                    successForm().append(new StringItem("Trxn Number: "+confNumber,null));
                }else{
                    successForm().append(new StringItem("Status: "+status,null));
                    successForm().append(new StringItem("Confirmation Number: "+confNumber,null));
                }
            }
            //generic information on successForm
            successForm().append(new StringItem("Guard ID: "+guardID,null));
            successForm().append(new StringItem("Shift: "+shift,null));
            successForm().append(new StringItem("Name: "+name,null));
            //successForm().append(new StringItem("Number: "+number,null));
            successForm().append(new StringItem("Site: "+location,null));
            //avoid saving transaction when from history or pending
            if(hisPendFlag==false){
                //save after successfuly connectining to server
                saveTransaction(activeTrxn.toString());
                successForm().removeCommand(getBackCommand());
                successForm().addCommand(newSubmissionCommand());
            }
            else{
                successForm().removeCommand(newSubmissionCommand());
                successForm().addCommand(getBackCommand());
            }
            switchDisplayable(null,successForm());
        }
        catch(Exception ex){
            showAlert(Strings.errorAlertTitle,ex.getMessage());
        }
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" createSuccessForm">
    public void createAttSuccessForm(String guardID,String action,String descr,String name,String number,String confNumber,String date,String checkPoint,String status){
        //this form is used to view pending, history and successful transactions
        try{
            attSuccessForm().deleteAll();
            attSuccessForm().append(new StringItem(date,null));
            //server connections fail
            if((connectionIsAvailable == false&&hisPendFlag==false)/*||(hisPendFlag == true&&historyFlag == false)*/){
                attSuccessForm().append(new StringItem("Transaction Incomplete, saved for later submission",null));
            }
            //server connections successful
            else if(connectionIsAvailable == true&&hisPendFlag==false){
                attSuccessForm().append(new StringItem("Transaction Successful",null));
                attSuccessForm().append(new StringItem("Confirmation Number: "+confNumber,null));
            }
            //coming from pending or history transactions
            else{
                if(pendingFlag==true){
                    attSuccessForm().append(new StringItem("Transaction Incomplete",null));
                    attSuccessForm().append(new StringItem("Trxn Number: "+confNumber,null));
                }else{
                    attSuccessForm().append(new StringItem("Status: "+status,null));
                    attSuccessForm().append(new StringItem("Confirmation Number: "+confNumber,null));
                }
            }
            //generic information on successForm
            attSuccessForm().append(new StringItem("Guard ID: "+guardID,null));
            attSuccessForm().append(new StringItem("Action: "+action,null));
            attSuccessForm().append(new StringItem("Descr: "+descr,null));
            attSuccessForm().append(new StringItem("Name: "+name,null));
            //attSuccessForm().append(new StringItem("NUMBER: ",number+"\n"));
            attSuccessForm().append(new StringItem("Site: "+checkPoint,null));
            //avoid saving transaction when from history or pending
            if(hisPendFlag==false){
                //save after successfuly connectining to server
                saveTransaction(activeTrxn.toString());
                attSuccessForm().removeCommand(getBackCommand());
                attSuccessForm().addCommand(newSubmissionCommand());
            }
            else{
                attSuccessForm().removeCommand(newSubmissionCommand());
                attSuccessForm().addCommand(getBackCommand());
            }
            switchDisplayable(null,attSuccessForm());
        }
        catch(Exception ex){
            showAlert("Error",ex.getMessage());
        }
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" createHistory">
    public void createHistory(){
        //creat history transactin choiceGroup
        RecordEnumeration re;
        try{
            historyPendingChoiceGroup().deleteAll();//delete previous data
            history = RecordStore.openRecordStore(Strings.historyStoreName, true);//open history store
            if(history.getNumRecords()>0){
                historyFlag = true;//flag distinguishes history from pending transactions
                re = history.enumerateRecords(null, null, false);// all history transactions
                //det is a vector that contains each History transactions details
                det.removeAllElements();//deletes previous details
                //recordIDs is a vector that stores all ids for histroy transactions as they occur in the record store 
                recordIDs.removeAllElements();//clear previous ids
                //int count = 1;
                while(re.hasNextElement()){//access each transaction one at a time
                    byte []bytes = re.nextRecord();
                    String record = new String(bytes,0,bytes.length);//represents a single history transaction
                    det = split(split(record,"?").elementAt(1).toString(),"*");//history transaction is a string concatinated by * therefore spliting by that
                    //sample record: 1?Attendance*BOGERE HUDSON*9991*NIGHT*783131266*1391156080*20003*STANBIC/Jinja_Main_Office*2014-01-31
                    //2?Schedule Shifts*JAMES OBO*STANBIC/Jinja_Main_Office*20*785678900*1391160030*20003*25*9991*2014-01-31
                    //Supervisor Tour*4545*343434*INCIDENT*broken glass*200214200120*9991*pending*2014-01-09
                    //Store Manager*ROLL*12001*OKETCH LIVINGSTONE*20*300114104024*1002*LUGAZI/SCOUL*9991*2014-01-09
                    String action = det.elementAt(0).toString();
                    //System.out.println(flg);
                    if(action.equals("Attendance")){
                        historyPendingChoiceGroup().append(det.elementAt(3)+"-"+det.elementAt(1), null);
                    }
                    else if(action.equals("Guard Tour")){
                        historyPendingChoiceGroup().append("Att:"+det.elementAt(3)+"-"+det.elementAt(1), null);
                    }
                    else if(action.equals("Schedule Shifts")){
                        historyPendingChoiceGroup().append("Schd-"+det.elementAt(2)+"-"+det.elementAt(3), null);
                    }
                    else if(action.equals("Supervisor Tour")){
                        historyPendingChoiceGroup().append("Ckpt-"+det.elementAt(1)+"-"+det.elementAt(3), null);
                    }
                    else if(action.equals("Store Manager")){
                        historyPendingChoiceGroup().append(split("Item-"+up.itemsHashtable.get(det.elementAt(2)).toString(),"_").elementAt(0) +" "+det.elementAt(4)+det.elementAt(1), null);
                    }
                    //historyPendingChoiceGroup().setFont(, getFont());
                    recordIDs.addElement(split(record,"?").elementAt(0).toString());//add id eg 1
                    //System.out.println(recordIDs);
                }
            }
            else{
                //historyPendingForm().removeCommand(viewCommand());
            }
            historyPendingChoiceGroup().setLabel(Strings.historyChoiceGroupName);
            historyPendingForm().setTitle(Strings.historyChoiceGroupName);
            history.closeRecordStore();
            pendingFlag = false;
            switchDisplayable(null,historyPendingForm());
        }
        catch(IndexOutOfBoundsException ex){
            ex.printStackTrace();
            showAlert("History Empty","No history!");
        }
        catch(RecordStoreException ex){
            showAlert("Recordstore Error",ex.getMessage());
        }                
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" createPending">
    public void createPending(){
        //creates pending choice group. Flow same as creatHistroy()
        try{
            historyPendingChoiceGroup().deleteAll();
            pending = RecordStore.openRecordStore("pending", true);
            if(pending.getNumRecords()>0){
                historyFlag = false;
                RecordEnumeration re = pending.enumerateRecords(null, null, false);
                det.removeAllElements();
                recordIDs.removeAllElements();
                int count = 0;
                while(re.hasNextElement()){
                    byte []bytes = re.nextRecord();
                    String record = new String(bytes,0,bytes.length);
                    det = split(split(record,"?").elementAt(1).toString(),"*");//history transaction is a string concatinated by * therefore spliting by that
                    //sample record: 1?0*BOGERE HUDSON*9991*NIGHT*783131266*1391156080*20003*STANBIC/Jinja_Main_Office*2014-01-31
                    String action = det.elementAt(0).toString();
                    //System.out.println(flg);
                    if(action.equals("Attendance")){
                        historyPendingChoiceGroup().append(det.elementAt(3)+"-"+det.elementAt(1), null);
                    }
                    else if(action.equals("Guard Tour")){
                        historyPendingChoiceGroup().append("Att:"+det.elementAt(3)+"-"+det.elementAt(1), null);
                    }
                    else if(action.equals("Schedule Shifts")){
                        historyPendingChoiceGroup().append("Schd-"+det.elementAt(2)+"-"+det.elementAt(3), null);
                    }
                    else if(action.equals("Supervisor Tour")){
                        historyPendingChoiceGroup().append("Ckpt-"+det.elementAt(1)+"-"+det.elementAt(3), null);
                    }
                    //1?Store Manager*Rolls*4002*Nice & Soft tissue paper*5*240214082750*6*SHOPPERS STOP PL/SHOPPERS STOP PL*9991*pending*2014-02-24
                    else if(action.equals("Store Manager")){
                        historyPendingChoiceGroup().append(split("Item-"+up.itemsHashtable.get(det.elementAt(2)).toString(),"_").elementAt(0) +" "+det.elementAt(4)+det.elementAt(1), null);
                    }
                    //historyPendingChoiceGroup().setFont(count, getFont());//set Font for currently added element
                    count++;
                    recordIDs.addElement(split(record,"?").elementAt(0).toString());
                }
            }
            historyPendingChoiceGroup().setLabel("Pending");
            historyPendingForm().setTitle("Pending");
            pending.closeRecordStore();
            pendingFlag = true;
            switchDisplayable(null,historyPendingForm());
        }
        catch(IndexOutOfBoundsException ex){
            ex.printStackTrace();
            showAlert("Zero Transactions","No pending Transactions!");
        }
        catch(RecordStoreException ex){
            showAlert("Recordstore Error",ex.getMessage());
        }
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" savePassword">
    private void savePassword(){
        //convert contents of passfield into bytes
        byte[] bytes = ((TextField)passWordForm.get(1)).getString().getBytes();
        try{
            String [] recordstores = RecordStore.listRecordStores();//lists all available record stores
            for(int x = 0; x<recordstores.length; x++){
                if("password".equals(recordstores[x])){//check if password exists
                    //delete currently existing password
                    RecordStore.deleteRecordStore(recordstores[x]);
                    break;
                }
            }
            //save new user details
            RecordStore rs3 = RecordStore.openRecordStore("password", true);
            rs3.addRecord(bytes, 0, bytes.length);
            rs3.closeRecordStore();
        }
        catch(RecordStoreException ex){
            showAlert("Recordstore Error",ex.getMessage());
        }   
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" getTrxnNumber">    
    public String getTrxnNumber() {
        String mnt = Strings.emptyString;
        String [] months = {"Jan","Feb","Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"};
        Vector dateInfo = split((new Date()).toString()," ");//Fri Nov 29 11:57:35 EAT 2013
        if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[0])){
            mnt = "01";
        }
        else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[1])){
            mnt = "02";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[2])){
            mnt = "03";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[3])){
            mnt = "04";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[4])){
            mnt = "05";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[5])){
            mnt = "06";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[6])){
            mnt = "07";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[7])){
            mnt = "08";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[8])){
            mnt = "09";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[9])){
            mnt = "10";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[10])){
            mnt = "11";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[11])){
            mnt = "12";
        }
        String txn = dateInfo.elementAt(2)+mnt+(dateInfo.elementAt(5).toString().substring(2, 4))+(dateInfo.elementAt(3).toString().substring(0, 2))+(dateInfo.elementAt(3).toString().substring(3, 5))+(dateInfo.elementAt(3).toString().substring(6, 8));
        return txn;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" getDateTime">    
    public static String getDateTime() {//Fri 29 Nov 2013
        String mnt = Strings.emptyString;
        String [] months = {"Jan","Feb","Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"};
        Vector dateInfo = split((new Date()).toString()," ");
        if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[0])){
            mnt = "01";
        }
        else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[1])){
            mnt = "02";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[2])){
            mnt = "03";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[3])){
            mnt = "04";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[4])){
            mnt = "05";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[5])){
            mnt = "06";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[6])){
            mnt = "07";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[7])){
            mnt = "08";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[8])){
            mnt = "09";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[9])){
            mnt = "10";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[10])){
            mnt = "11";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[11])){
            mnt = "12";
        }
        String txn = dateInfo.elementAt(5).toString()+"-"+mnt+"-"+dateInfo.elementAt(2);
        return txn;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" saveTransaction"> 
    public void saveTransaction(String activeTrxin){
        //activeTrxn: MBONYE PHILIP*9990*DAY*781191914*1385714799*Fri 29 Nov 2013
        try{
            RecordStore rst;
            if(connectionIsAvailable==false){
                rst = RecordStore.openRecordStore("pending", true);
            }
            else{
                rst = RecordStore.openRecordStore(Strings.historyStoreName, true);
            }
            int idt = rst.getNextRecordID();
            String transaction = idt+"?"+activeTrxin;//16?MBONYE PHILIP*9990*DAY*781191914*1385714799*Fri 29 Nov 2013                                                                              
            //0*BOGERE HUDSON*9991*NIGHT*783131266*1391156080*20003*STANBIC/Jinja_Main_Office*2014-01-31
            byte [] bytes1 = transaction.getBytes();
            rst.addRecord(bytes1, 0, bytes1.length);
            rst.closeRecordStore();
            activeTrxn.delete(0, activeTrxn.length());
            System.out.println(transaction);
        }
        catch(RecordStoreException ex){
            showAlert("Recordstore Error",ex.getMessage());
        }
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" decodeTransaction">
public void decodeTransaction(String recordID,String recordStoreName){ // creates back transaction saved as history from RecordSore object to string object
    hisPendFlag = true;
    //historyFlag = true;
    try{
        RecordStore rs = RecordStore.openRecordStore(recordStoreName, true);
        byte []bytes = rs.getRecord(Integer.parseInt(recordID));
        String record = new String(bytes,0,bytes.length);
        //System.out.println(record);
        Vector trxn = split(split(record,"?").elementAt(1).toString(),"*");//16?MBONYE PHILIP*9990*DAY*781191914*1385714799*Fri 29 Nov 2013
        //0*BOGERE HUDSON*9991*DAY*783131266*1391331348*20003*STANBIC/Jinja_Main_Office*9991*2014-02-02
        //1*JAMES OBO*STANBIC/Jinja_Main_Office*20*785678900*1391160030*20003*25*9991*2014-01-31
        //2*OKETCH LIVINGSTONE*12345*9000*INCIDENT*come now*300114104024*9991*2014-01-09
        String action = trxn.elementAt(0).toString();
        if(action.equals("Attendance")){
            //createSuccessForm(guardID,shift,name,number,confNumber,date,location)
            createSuccessForm(trxn.elementAt(2).toString(),trxn.elementAt(3).toString(),trxn.elementAt(1).toString(),trxn.elementAt(4).toString(),trxn.elementAt(5).toString(),trxn.elementAt(10).toString(),trxn.elementAt(7).toString(),trxn.elementAt(9).toString());
            successForm().setTitle("Attendance");
        }
        if(action.equals("Guard Tour")){
            //createSuccessForm(guardID,shift,name,number,confNumber,date,location)
            createAttSuccessForm(trxn.elementAt(2).toString(),trxn.elementAt(3).toString(),trxn.elementAt(10).toString(),trxn.elementAt(1).toString(),trxn.elementAt(4).toString(),trxn.elementAt(5).toString(),trxn.elementAt(11).toString(),trxn.elementAt(7).toString(),trxn.elementAt(9).toString());
            attSuccessForm().setTitle("CP-Attendance");
        }
        else if(action.equals("Schedule Shifts")){
            //createShiftsSuccessForm(date,confNumber,location,officer,phone,expShifts)
            createShiftsSuccessForm(trxn.elementAt(trxn.size()-1).toString(),trxn.elementAt(5).toString(),trxn.elementAt(2).toString(),trxn.elementAt(1).toString(),trxn.elementAt(4).toString(),trxn.elementAt(3).toString(),trxn.elementAt(9).toString());
            shiftsSuccessForm().setTitle("Schedule Shifts");
        }
        else if(action.equals("Supervisor Tour")){
            //Supervisor Tour*7878*000000*CLOCK IN*n/a*110214083430*9991*2014-01-24
            //createTourSuccessForm(today, confNum, chkPtID,actn, descrip)
            createTourSuccessForm(trxn.elementAt(trxn.size()-1).toString(),trxn.elementAt(5).toString(),trxn.elementAt(1).toString(),trxn.elementAt(3).toString(),trxn.elementAt(4).toString(),trxn.elementAt(7).toString());
            tourSuccessForm().setTitle("Submit Tour");
        }
        else if(action.equals("Store Manager")){
            //3*ROLL*12001*OKETCH LIVINGSTONE*20*300114104024*1002*LUGAZI/SCOUL*9991*2014-01-09
            //createItemSuccessForm(date,confNumber,location,item,unit,qty);
            //System.out.println(trxn.elementAt(5).toString());
            createItemSuccessForm(trxn.elementAt(trxn.size()-1).toString(),trxn.elementAt(5).toString(),trxn.elementAt(7).toString(),trxn.elementAt(3).toString(),trxn.elementAt(1).toString(),trxn.elementAt(4).toString(),trxn.elementAt(9).toString());
            itemSuccessForm().setTitle("Submit Item");
        }
        /*try{
            Integer.parseInt(trxn.elementAt(1).toString());
            String lctn = split(up.locationsHashTable.get(trxn.elementAt(5).toString()).toString(),"#").elementAt(3).toString();
            createSuccessForm(trxn.elementAt(1).toString(),trxn.elementAt(2).toString(),trxn.elementAt(0).toString(),trxn.elementAt(3).toString(),trxn.elementAt(4).toString(),trxn.elementAt(6).toString(),lctn);
            successForm().setTitle("Guard Manager");
        }
        catch(NumberFormatException ex){
            createShiftsSuccessForm(trxn.elementAt(trxn.size()-1).toString(),trxn.elementAt(4).toString(),trxn.elementAt(1).toString(),trxn.elementAt(0).toString(),trxn.elementAt(3).toString(),trxn.elementAt(2).toString());
            shiftsSuccessForm().setTitle("Guard Manager");
        }*/
        
        rs.closeRecordStore();
    }
    catch(RecordStoreException ex){
        ex.printStackTrace();
        showAlert("Recordstore Error",ex.getMessage());
    }
    catch(Exception ex){
        ex.printStackTrace();
        showAlert(Strings.errorAlertTitle,ex.getMessage());
    }
} 
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" resetForms">    
    private void resetForms(){
        try{
            if(!(confirmDetails == null)){
                confirmDetails.deleteAll();
            }
            if(!(historyPendingForm == null)){
                //historyPendingForm.deleteAll();
            }
            if(!(successForm == null)){
                successForm.deleteAll();
            }
            if(!(idEntryForm  == null)){
                //idEntryForm.deleteAll();
            }
            if(!(waitForm == null)){
                waitForm.deleteAll();
            }
            //successWaitForm = null;
        }
        catch(Exception ex){
            showAlert(Strings.errorAlertTitle,ex.toString());
        }
    }
//</editor-fold>
  
//<editor-fold defaultstate="collapsed" desc=" validateField">    
    /*private boolean validateField(String field){
        boolean passes = false;
        int fieldLength = field.length();
        try{
            if(((fieldLength<=4&&fieldLength>0)&&pinFlag==false)||(fieldLength==4&&pinFlag==true)||fieldLength>0&&flag == 1){
                passes = true;
            }
            else{
                if(pinFlag==false){
                    showAlert("Staff ID Error","Staff ID must be 1-4 characters long!");
                }
                else if(flag == 1){
                    showAlert("Field Error","provide number of shifts to continue");
                }
                else{
                    showAlert("Pin code Error","pin code must be 4 characters long!");
                }
                passes = false;
            }
        }
        catch(Exception ex){
            showAlert("Error",ex.toString());
        }
        return passes;
    }*/
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" font ">
    /**
     * Returns an initialized instance of font component.
     *
     * @return the initialized component instance
     */
    public Font getFont() {
        if (font == null) {
            // write pre-init user code here
            font = Font.getFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_SMALL);
            // write post-init user code here
        }
        return font;
    }
    //</editor-fold>

//<editor-fold defaultstate="collapsed"  desc=" pinTextBox">
    private TextBox tourPinTextBox() {
        if (tourPinTextBox == null) {
            tourPinTextBox = new TextBox(Strings.passwordTextBoxTitle, null, 5, TextField.NUMERIC);
            tourPinTextBox.addCommand(okCommand());
            tourPinTextBox.addCommand(getBackCommand());
            tourPinTextBox.setCommandListener(this);
        }
        return tourPinTextBox;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed"  desc=" pinTextBox">
    private TextBox optPinTextBox() {
        if (optPinTextBox == null) {
            optPinTextBox = new TextBox("Enter Token OTP", null, 7, TextField.NUMERIC);
            optPinTextBox.addCommand(okCommand());
            optPinTextBox.addCommand(getBackCommand());
            optPinTextBox.setCommandListener(this);
        }
        return optPinTextBox;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" textBox shiftPinTextBox">
    private TextBox shiftPinTextBox() {
        if (shiftPinTextBox == null) {
            shiftPinTextBox = new TextBox(Strings.passwordTextBoxTitle, null, 5, TextField.NUMERIC);
            shiftPinTextBox.addCommand(okCommand());
            shiftPinTextBox.addCommand(getBackCommand());
            shiftPinTextBox.setCommandListener(this);
        }
        return shiftPinTextBox;
    }
//</editor-fold> 
    
//<editor-fold defaultstate="collapsed" desc=" schedulePinTextBox">
    private TextBox schedulePinTextBox() {
        if (schedulePinTextBox == null) {
            schedulePinTextBox = new TextBox(Strings.passwordTextBoxTitle, null, 5, TextField.NUMERIC);
            schedulePinTextBox.addCommand(okCommand());
            schedulePinTextBox.addCommand(getBackCommand());
            schedulePinTextBox.setCommandListener(this);
        }
        return schedulePinTextBox;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" textBox itemPinTextBox">
    private TextBox itemPinTextBox() {
        if (itemPinTextBox == null) {
            itemPinTextBox = new TextBox(Strings.passwordTextBoxTitle, null, 5, TextField.NUMERIC);
            itemPinTextBox.addCommand(okCommand());
            itemPinTextBox.addCommand(getBackCommand());
            itemPinTextBox.setCommandListener(this);
        }
        return itemPinTextBox;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" passwordTextBox ">
    private TextBox passwordTextBox() {
        if (passwordTextBox == null) {
            passwordTextBox = new TextBox(Strings.passwordTextBoxTitle, null, 6, TextField.NUMERIC);
            passwordTextBox.addCommand(okCommand());
            passwordTextBox.addCommand(getExitCommand());
            passwordTextBox.setCommandListener(this);
        }
        return passwordTextBox;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" passwordTextBox ">
    private TextBox scheduleShiftPasswordTextBox() {
        if (scheduleShiftPasswordTextBox == null) {
            scheduleShiftPasswordTextBox = new TextBox(Strings.passwordTextBoxTitle, null, 5, TextField.NUMERIC);
            scheduleShiftPasswordTextBox.addCommand(okCommand());
            scheduleShiftPasswordTextBox.addCommand(getBackCommand());
            scheduleShiftPasswordTextBox.setCommandListener(this);
        }
        return scheduleShiftPasswordTextBox;
    }
//</editor-fold>    
 
//<editor-fold defaultstate="collapsed" desc="actionsCheckPointIDTextBox">
    private TextField actionsCheckPointIDTextBox() {
        if (actionsCheckPointIDTextBox == null) {                                 
            actionsCheckPointIDTextBox = new TextField("Chkpt ID","", 5, TextField.NUMERIC);
            //actionsCheckPointIDTextBox.addCommand(okCommand());
            //actionsCheckPointIDTextBox.addCommand(cancelCommand());
            //actionsCheckPointIDTextBox.setCommandListener(this);
        }                         
        return actionsCheckPointIDTextBox;
    }
//</editor-fold>     
   
//<editor-fold defaultstate="collapsed" desc=" actionsPINTextBox ">                                   
    public TextField actionsPINTextBox(){
        if (actionsPINTextBox == null) {                                 
            actionsPINTextBox = new TextField("PIN","", 5, TextField.NUMERIC);
            //actionsPINTextBox.addCommand(okCommand());
            //actionsPINTextBox.addCommand(cancelCommand());
            //actionsPINTextBox.addCommand(getBackCommand());
            //actionsPINTextBox.setCommandListener(this);
        }                         
        return actionsPINTextBox;
    }
//</editor-fold> 
    
//<editor-fold defaultstate="collapsed" desc=" actionsOTPTextBox ">                                   
    public TextField actionsOTPTextBox(){
        if (actionsOTPTextBox == null) {                                 
            actionsOTPTextBox = new TextField("OTP","", 6, TextField.NUMERIC);
            //actionsOTPTextBox.addCommand(okCommand());
            //actionsOTPTextBox.addCommand(getBackCommand());
            //actionsOTPTextBox.addCommand(cancelCommand());
            //actionsOTPTextBox.setCommandListener(this);
        }                         
        return actionsOTPTextBox;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" staffIDTextBox ">                                   
    public TextField staffIDTextBox() {
        if (staffIDTextBox == null) {                                 
            staffIDTextBox = new TextField("Guard ID","", 6, TextField.NUMERIC);
            //staffIDTextBox.addCommand(okCommand());
            //staffIDTextBox.addCommand(getBackCommand());
            //staffIDTextBox.addCommand(cancelCommand());
            //staffIDTextBox.setCommandListener(this);
        }                         
        return staffIDTextBox;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" showAlert ">
    public void showAlert(String title,String msg){
        getAlert().setString(msg);
        getAlert().setTitle(title);
        switchDisplayable(null,getAlert());
    }
//</editor-fold>  
    
//<editor-fold defaultstate="collapsed" desc=" getDisplay">
    public Display getDisplay() {
        return Display.getDisplay(this);
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" exitMIDlet">
    /**
     * Exits MIDlet.
     */
    public void exitMIDlet() {
        //kill all background threads first 
        timer.cancel();
        //timer1.cancel();
        timer2.cancel();
        switchDisplayable(null, null);
        destroyApp(true);
        notifyDestroyed();
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" startApp">
    /**
     * Called when MIDlet is started. Checks whether the MIDlet have been
     * already started and initialize/starts or resumes the MIDlet.
     */
    public void startApp() {
        if (midletPaused) {
            resumeMIDlet();
        } else {
            initialize();
            startMIDlet();
        }
        midletPaused = false;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" pauseApp">    
    /**
     * Called when MIDlet is paused.
     */
    public void pauseApp() {
        midletPaused = true;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" destroyApp">    
    /**
     * Called to signal the MIDlet to terminate.
     *
     * @param unconditional if true, then the MIDlet has to be unconditionally
     * terminated and all resources has to be released.
     */
    public void destroyApp(boolean unconditional) {
        notifyDestroyed();
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" itemStateChanged">
    public void itemStateChanged(Item item) {
        if(item instanceof TextField /*&& (((TextField)item)==itemNameTextField||((TextField)item)==searchTextField)*/) {
            String text = ((TextField)item).getString();
            String label = ((TextField)item).getLabel();
            if(label.equals("Search")){
                locationsChoiceGroup.filter(text, itemsVector, itemsChoiceGroup);
            }
            else{
                locationsChoiceGroup.filter(text, getLocations(), locationsChoiceGroup);
            }
        }
    }
    //</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" commandAction ">
    public void commandAction(Command c, Item item) {
        
    }
//</editor-fold>
  
//<editor-fold defaultstate="collapsed" desc=" SendPendingTask"> 
    class  SendPendingTask  extends TimerTask{
        public void run() {
            //action to be performed
            //System.out.println("sending pending");
            try{
                RecordStore pendingRecordStore = RecordStore.openRecordStore("pending", true);
                //when there are pending transactions
                if(!(pendingRecordStore.getNumRecords()==0)){
                    RecordEnumeration re = pendingRecordStore.enumerateRecords(null, null, false);
                    byte [] bytes = null;
                    while(re.hasNextElement()){
                        bytes = re.nextRecord();
                        break;
                    }
                    String trxn = new String(bytes,0,bytes.length);
                    Vector pendingTrxn = split(trxn,"?");
                    //1?0*BOGERE HUDSON*9991*NIGHT*783131266*1391156080*20003*STANBIC/Jinja_Main_Office*1234*2014-01-31
                    //2?1*JAMES OBO*STANBIC/Jinja_Main_Office*20*785678900*1391160030*20003*25*9991*2014-01-31
                    //1?Supervisor Tour*5609*565656*CLOCK IN*n/a*110214085341*9991*2014-01-24
                    //4?3*ROLL*12001*OKETCH LIVINGSTONE*20*300114104024*1002*LUGAZI/SCOUL*9991*2014-01-09
                    pendingRecordID = Integer.parseInt(pendingTrxn.elementAt(0).toString());
                    Vector trans = split(pendingTrxn.elementAt(1).toString(),"*");
                    //0*BOGERE HUDSON*9991*NIGHT*783131266*1391156080*20003*STANBIC/Jinja_Main_Office*1234*2014-01-31
                    //1*JAMES OBO*STANBIC/Jinja_Main_Office*20*785678900*1391160030*20003*25*9991*2014-01-31
                    //2*OKETCH LIVINGSTONE*12345*9000*INCIDENT*come now*300114104024*9991*2014-01-09
                    //3*ROLL*12001*OKETCH LIVINGSTONE*20*300114104024*1002*LUGAZI/SCOUL*9991*2014-01-09
                    String action = trans.elementAt(0).toString();
                    if(action.equals("Schedule Shifts")){//Schedule Shifts
                        //setTransaction(location,locationID,expShifts,pin,transNum,adj)
                        //sp.setTransaction(trans.elementAt(2).toString(),trans.elementAt(6).toString(), Integer.parseInt(trans.elementAt(3).toString()), trans.elementAt(8).toString(),trans.elementAt(5).toString(),trans.elementAt(7).toString(),trans.elementAt(1).toString(),trans.elementAt(4).toString(),action);
                        //sp.startConnection();
                    }
                    else if(action.equals("Attendance")){//Shift Submission
                        //setTransaction(name,id,shift,pin,location,locationID,trxnNum)
                        sp.setTransaction(trans.elementAt(1).toString(), trans.elementAt(2).toString(), trans.elementAt(3).toString(), trans.elementAt(8).toString(), trans.elementAt(7).toString(), trans.elementAt(6).toString(),trans.elementAt(5).toString(),action,url,imei);
                        sp.startConnection();
                    }
                    /*else if(action.equals("Guard Tour")){//Shift Submission
                        //setTransaction(name,id,shift,pin,location,locationID,trxnNum)
                        sp.setTransaction(trans.elementAt(1).toString(), trans.elementAt(2).toString(), trans.elementAt(3).toString(), trans.elementAt(8).toString(), trans.elementAt(7).toString(), trans.elementAt(6).toString(),trans.elementAt(5).toString(),action);
                        sp.startConnection();
                    }*/
                    else if(action.equals("Supervisor Tour")){//Supervisor Tour
                        //Supervisor Tour*5609*565656*CLOCK IN*n/a*110214085341*9991*2014-01-24
                        //setTransaction(opt,chkptID,action,desc, pin,transNum,pendingState)
                        //sp.setTransaction(trans.elementAt(2).toString(), trans.elementAt(1).toString(), trans.elementAt(3).toString(), trans.elementAt(4).toString(), trans.elementAt(6).toString(), trans.elementAt(5).toString(),action);
                        //sp.startConnection();
                    }
                    else if(action.equals("Store Manager")){//Store manager
                        //3*ROLL*12001*OKETCH LIVINGSTONE*20*300114104024*1002*LUGAZI/SCOUL*9991*2014-01-09
                        //setTransaction(location,locationID,itemID,itemName,itemUnit,itemQty, pin,transNum)
                        //sp.setTransaction(trans.elementAt(7).toString(), trans.elementAt(6).toString(), trans.elementAt(2).toString(), trans.elementAt(3).toString(),trans.elementAt(1).toString(), trans.elementAt(4).toString(),trans.elementAt(8).toString(), trans.elementAt(5).toString(),action);
                        //sp.startConnection();
                    }
                }
                pendingRecordStore.closeRecordStore();
                //timer.cancel();
            }
            catch(Exception ex){
                //ex.printStackTrace();
            }
        }
    
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" pendingSheduler">    
    private void pendingSheduler() {
        timer = new Timer();
        try{
            //thread wakes up 1 min (60000) after the start of app to start sending pending transactions 
            timer.scheduleAtFixedRate(new Attendance.SendPendingTask(), 60000,1*120000);
            // every after 2 min (120000) one pending transaction is sent to the server until they are all done
        }
        catch(IllegalStateException ex){
        }
        catch(IllegalArgumentException ex){
        }
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" updateTask">
    class  UpdateTask  extends TimerTask{

        public void run() {
            up.startUpdate(pin,imei,url);// start update thread
        }
        
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" updateSheduler">
    private void updateSheduler() {
        timer1 = new Timer();
        try{
            //wakes up 5 min after the start of app and updates every after 5 min
            timer1.scheduleAtFixedRate(new Attendance.UpdateTask(), 300000,1*300000);
        }
        catch(IllegalStateException ex){
        }
        catch(IllegalArgumentException ex){
        }
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" historyDeleteTask">
    class  HistoryDeleteTask  extends TimerTask{

        public void run() {
            try{
                RecordStore history = RecordStore.openRecordStore(Strings.historyStoreName, true);
                if(!(history.getNumRecords()==0)){
                    RecordEnumeration re = history.enumerateRecords(null, null, false);
                    byte [] bytes;
                    Vector historyTrxn = new Vector();
                    String datetime;
                    String trxn;
                    String dat;
                    int date,currentDate,recordid;
                    while(re.hasNextElement()){
                        historyTrxn.removeAllElements();
                        bytes = re.nextRecord();
                        trxn = new String(bytes,0,bytes.length);
                        //[MBONYE PHILIP, 9990, DAY, 781191914, 1387291520, 2014-01-07]
                        historyTrxn = split((split(trxn,"?").elementAt(1).toString()),"*");
                        recordid = Integer.parseInt((split(trxn,"?").elementAt(0).toString()));
                        //System.out.println(historyTrxn);
                        //System.out.println(getDateTime());
                        datetime = historyTrxn.elementAt(historyTrxn.size()-1).toString();
                        //System.out.println(datetime);
                        dat = (split(datetime,"-").elementAt(2).toString());
                        //prevMonth = (split(datetime," ").elementAt(2).toString());
                        date = Integer.parseInt(dat);
                        //System.out.println("prev="+date);
                        //2014-01-07
                        currentDate = Integer.parseInt((split(up.today,"-").elementAt(2).toString()));
                        //currMonth = (split((new Date()).toString()," ").elementAt(1).toString());
                        //System.out.println("curr="+currentDate);
                        if(date<currentDate||(date>currentDate&&currentDate==1)){
                            history.deleteRecord(recordid);
                        }
                         //System.out.println(hr+currentHour);
                    }
                }
            }
            catch(Exception ex){
                
            }
            //timer2.cancel();
        }
        
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" deleteSheduler">
    private void deleteSheduler() {
        timer2 = new Timer();
        try{
            timer2.scheduleAtFixedRate(new Attendance.HistoryDeleteTask(), 60000,1*30000);
        }
        catch(IllegalStateException ex){
        }
        catch(IllegalArgumentException ex){
        }
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" serverConnectionSheduler">
    public void serverConnectionSheduler() {
        serverConnectionTimer = new Timer();
        try{
            serverConnectionTimer.schedule(new Attendance.ServerConnectionTask(), 30000);
        }
        catch(IllegalStateException ex){
        }
        catch(IllegalArgumentException ex){
        }
    }
//</editor-fold>   
    
//<editor-fold defaultstate="collapsed" desc=" ServerConnectionTask">
    class  ServerConnectionTask  extends TimerTask{

        public void run() {
            try{
                if(cts.status==null){
                    getAlert().setType(AlertType.ERROR);
                    getAlert().setTitle("Connection Error");
                    getAlert().setString("Connection time out!");
                    switchDisplayable(getAlert(),state);
                }
            }
            catch(Exception ex){
                
            }
            serverConnectionTimer.cancel();
        }  
    }  
//</editor-fold>
 
//<editor-fold defaultstate="collapsed" desc=" getLocations ">
    private Vector getLocations(){
        Vector locations = new Vector();
        try{
            System.out.println(up.locationsHashTable.toString());
            Enumeration locationsEnum = up.locationsHashTable.keys();
            String locationName,locationID;
            while(locationsEnum.hasMoreElements()){
                locationID = locationsEnum.nextElement().toString();
                Vector locationsData = split(up.locationsHashTable.get(locationID).toString(),"#");
                locationName = locationsData.elementAt(locationsData.size()-1).toString();
                locations.addElement(locationName);
                locationANDid.put(locationName, locationID);
            }
        }
        catch(Exception ex){
            getAlert().setType(AlertType.ERROR);
            getAlert().setTitle(Strings.errorAlertTitle);
            getAlert().setString(ex.toString());
            //ex.printStackTrace();
            switchDisplayable(getAlert(),mainMenu());
        }
        return locations;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" getItems ">    
    private Vector getItems(){
        Vector items = new Vector();
        try{
            System.out.println(up.locationsHashTable.toString());
            Enumeration locationsEnum = up.itemsHashtable.keys();
            String item,itemId;
            while(locationsEnum.hasMoreElements()){
                itemId = locationsEnum.nextElement().toString();
                Vector itemData = split(up.itemsHashtable.get(itemId).toString(),"_");
                item = itemData.elementAt(0).toString();
                items.addElement(item);
                itemANDid.put(item, itemId);
            }
        }
        catch(Exception ex){
            getAlert().setType(AlertType.ERROR);
            getAlert().setTitle(Strings.errorAlertTitle);
            getAlert().setString(ex.toString());
            //ex.printStackTrace();
            switchDisplayable(getAlert(),mainMenu());
        }
        return items;
    }
//</editor-fold>
    
}
